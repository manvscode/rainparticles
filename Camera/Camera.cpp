#include <windows.h>
#include <GL/GL.h>
#include <GL/GLU.h>
#include "Camera.h"

CCamera::CCamera( )
{
}

CCamera::~CCamera( )
{
}

void CCamera::setView( const CVector3<float> &position, const CVector3<float> &forward, const CVector3<float> &up )
{
	viewMatrix[ 0 ] = ;
	viewMatrix[ 1 ] = ;
	viewMatrix[ 2 ] = ;
	viewMatrix[ 3 ] = ;


	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;

	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;
	viewMatrix[ 0 ] = ;


	viewMatrix[ 12 ] = ;
	viewMatrix[ 13 ] = ;
	viewMatrix[ 14 ] = ;
	viewMatrix[ 15 ] = ;

	m_vcPosition = position;
	m_vcForward = forward;
	m_vcUp = up;
}

void CCamera::rotateCamera( float angle, float x, float y, float z )
{
	CVector3<float> vNewView;

	float cosTheta = (float) cos( angle );
	float sinTheta = (float) sin( angle );

	vNewView.x  = (cosTheta + (1 - cosTheta) * x * x)		* m_vcForward.getX( );
	vNewView.x += ((1 - cosTheta) * x * y - z * sinTheta)	* m_vcForward.getY( );
	vNewView.x += ((1 - cosTheta) * x * z + y * sinTheta)	* m_vcForward.getZ( );

	vNewView.y  = ((1 - cosTheta) * x * y + z * sinTheta)	* m_vcForward.getX( );
	vNewView.y += (cosTheta + (1 - cosTheta) * y * y)		* m_vcForward.getY( );
	vNewView.y += ((1 - cosTheta) * y * z - x * sinTheta)	* m_vcForward.getZ( );

	vNewView.z  = ((1 - cosTheta) * x * z - y * sinTheta)	* m_vcForward.getX( );
	vNewView.z += ((1 - cosTheta) * y * z + x * sinTheta)	* m_vcForward.getY( );
	vNewView.z += (cosTheta + (1 - cosTheta) * z * z)		* m_vcForward.getZ( );

	m_vcForward = vNewView;
}


void CCamera::viewingTransform( ) const
{
	gluLookAt( m_vcPosition.getX( ), m_vcPosition.getY( ), m_vcPosition.getZ( ),
		m_vcForward.getX( ), m_vcForward.getY( ), m_vcForward.getZ( ),
		m_vcUp.getX( ), m_vcUp.getY( ), m_vcUp.getZ( ) );
}