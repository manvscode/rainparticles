#ifndef _CAMERA_H_
#define _CAMERA_H_


#include "Vector3.h"
using namespace MATH;


class CCamera
{
  public:
	typedef enum tagVector {
		VT_FORWARD, VT_UP, VT_RIGHT
	} Vector;
	//CVector3<float> m_vcForward;
	//CVector3<float> m_vcUp;
	//CVector3<float> m_vcPosition;
	float viewMatrix[ 16 ];

	
  public:
	CCamera( );
	virtual ~CCamera( );

	void setView( const CVector3<float> &position, const CVector3<float> &forward, const CVector3<float> &up );
	virtual void rotateCamera( float angle, float x, float y, float z );
	//CVector3<float> get
	void viewingTransform( ) const;
};



#endif