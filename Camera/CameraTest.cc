/*
 *	Camera.cc
 * 
 *	A quaternion camera.
 *
 *	Coded by Joseph A. Marrero
 *	5/25/2007
 */
#include <cassert>
#include <iostream>
#include "CameraTest.h"
#include <GL/glut.h>

using namespace std;

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( 640, 480 );
	
	glutCreateWindow( "Camera" );
	//glutFullScreen( );

	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutMouseFunc( mouseHandler );
	glutMotionFunc( mouseMotionHandler ); 
	glutIdleFunc( idle );

	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}


void initialize( )
{
	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	//glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	//glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );		
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_DEPTH_TEST );


	glEnable( GL_LIGHTING );
	glEnable( GL_LIGHT0 );

	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	// TO DO: Initialization code goes here...
	theCamera.setPosition( 0.0f, 0.0f, 10.0f );
	theCamera.setUp( 0.0f, 1.0f, 0.0f );
	theCamera.setView( 0.0f, 0.0f, 0.0f );
}

void deinitialize( )
{
	// TO DO: Deinitialization code goes here...
}


void render( )
{
	glClearColor( 0.3f, 0.3f, 0.3f, 0.3f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity( );
	

	// TO DO: Drawing code goes here...
	theCamera.viewingTransform( );


	glColor3f( 1.0f, 0.0f, 0.0f );
	//glutSolidTetrahedron( );
	glutSolidTeapot( 2.0f );


	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	//writeText( GLUT_BITMAP_HELVETICA_18, std::string("Camera"), 2, 22 );
	//writeText( GLUT_BITMAP_9_BY_15, std::string("Press Q to quit."), 2, 5 );
	
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	gluPerspective( 65.0f, (double) width / (double) height, 2.0, 100.0 );
	glMatrixMode( GL_MODELVIEW );
}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		default:
			break;
	}

}

void mouseHandler( int button, int stat, int x, int y )
{
	Mouse.oldX = x;
	Mouse.oldY = y;
}

void mouseMotionHandler( int x, int y )
{
	int middleX = GLUT_SCREEN_WIDTH / 2;
	int middleY = GLUT_SCREEN_HEIGHT / 2;
	float dx = middleX - (x /*- Mouse.oldX*/);
	float dy = middleY - (y /*- Mouse.oldY*/);

	dx /= 4.0f;
	dy /= 4.0f;


	CVector3<float> right = crossProduct( theCamera.m_vcForward, theCamera.m_vcUp );
	right.normalize( );

	theCamera.rotateCamera( dy, right.getX( ), right.getY( ), right.getZ( ) );
	theCamera.rotateCamera( dx, 0.0f, 1.0f, 0.0f );

	Mouse.oldX = x;
	Mouse.oldY = y;
}

void idle( )
{ glutPostRedisplay( ); }

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glDisable( GL_DEPTH_TEST );
	//glDisable( GL_TEXTURE_2D );
	glDisable( GL_LIGHTING );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, width, 0, height, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glRasterPos2i( x, y );

			for( unsigned int i = 0; i < text.size( ); i++ )
				glutBitmapCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	glEnable( GL_LIGHTING );
	glEnable( GL_DEPTH_TEST );
	//glEnable( GL_TEXTURE_2D );

}
