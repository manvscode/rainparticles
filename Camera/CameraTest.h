#ifndef _CAMERATEST_H_
#define _CAMERATEST_H_
/*
 *	Camera.h
 *
 *	A quaternion camera.
 *
 *	Coded by Joseph A. Marrero
 *	5/25/2007
 */
#include <string>
#include "QCamera.h"
using namespace std;


void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void mouseHandler( int button, int stat, int x, int y );
void mouseMotionHandler( int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y );

CQCamera theCamera;

struct tagMouse {
	int oldX, oldY;
} Mouse;
#endif
