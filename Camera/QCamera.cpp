#include "QCamera.h"

CQCamera::CQCamera( )
 : CCamera( )
{
}

CQCamera::~CQCamera( )
{
}



void CQCamera::setView( float x, float y, float z )
{
	m_vcForward.setX( x );
	m_vcForward.setY( y );
	m_vcForward.setZ( z );
}

void CQCamera::setUp( float x, float y, float z )
{
	m_vcUp.setX( x );
	m_vcUp.setY( y );
	m_vcUp.setZ( z );
}

void CQCamera::setPosition( float x, float y, float z )
{
	m_vcPosition.setX( x );
	m_vcPosition.setY( y );
	m_vcPosition.setZ( z );
}

void CQCamera::rotateCamera( float angle, float axisX, float axisY, float axisZ )
{	
	float temp = (float) sin( angle / 2.0f );
	m_ViewQuaternion.set( m_vcForward.getX( ), m_vcForward.getY( ), m_vcForward.getZ( ), 0.0f );
	m_RotationQuaternion.set( axisX * temp, axisY * temp, axisZ * temp, (float) cos( angle / 2.0f ) );

	CQuaternion<float> W = m_RotationQuaternion * m_ViewQuaternion;
	m_RotationQuaternion.conjugate( );
	m_ViewQuaternion = W * m_RotationQuaternion;

	m_vcForward.setX( m_ViewQuaternion.x );
	m_vcForward.setY( m_ViewQuaternion.y );
	m_vcForward.setZ( m_ViewQuaternion.z );
}