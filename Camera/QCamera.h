#ifndef _QCAMERA_H_
#define _QCAMERA_H_

#include "Quaternion.h"
#include "Camera.h"
using namespace MATH;


class CQCamera : public CCamera
{
  protected:
	CQuaternion<float> m_ViewQuaternion;
	CQuaternion<float> m_RotationQuaternion;
	
  public:
	CQCamera( );
	virtual ~CQCamera( );

	void setView( float x, float y, float z );
	void setUp( float x, float y, float z );
	void setPosition( float x, float y, float z );
	void rotateCamera( float angle, float axisX, float axisY, float axisZ );
};


#endif