#ifndef _QUATERNION_H_
#define _QUATERNION_H_

namespace MATH {

template <class AnyType>
class CQuaternion
{
  public:
	union {
		struct { AnyType x, y, z, w; };
		AnyType v[ 4 ];
	};
	

  public:
	explicit CQuaternion( );
	CQuaternion( AnyType a, AnyType b, AnyType c, AnyType d );
	~CQuaternion( );

	void set( AnyType a, AnyType b, AnyType c, AnyType d );

	AnyType length( ) const;
	void normalize( );
	void conjugate( );
	void negate( );
};

template <class AnyType>
inline AnyType CQuaternion<AnyType>::length( ) const
{ return sqrt( x * x + y * y + z * z + w * w ); }

template <class AnyType>
inline void CQuaternion<AnyType>::normalize( )
{
	AnyType &L = length( );
	x /= L;
	y /= L;
	z /= L;
	w /= L;
}

template <class AnyType>
inline void CQuaternion<AnyType>::conjugate( )
{
	x = -x;
	y = -y;
	z = -z;
}

template <class AnyType>
inline void CQuaternion<AnyType>::negate( )
{
	x = -x;
	y = -y;
	z = -z;
	w = -w;
}

template <class AnyType>
CQuaternion<AnyType> operator+( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 );

template <class AnyType>
CQuaternion<AnyType> operator-( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 );

template <class AnyType>
CQuaternion<AnyType> operator*( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 );

#include "Quaternion.inl"
} // end of namespace...
#endif