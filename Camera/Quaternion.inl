template <class AnyType>
CQuaternion<AnyType>::CQuaternion( )
  : x(), y(), z(), w()
{
}

template <class AnyType>
CQuaternion<AnyType>::CQuaternion( AnyType a, AnyType b, AnyType c, AnyType d )
  : x(a), y(b), z(c), w(d)
{
}

template <class AnyType>
CQuaternion<AnyType>::~CQuaternion( )
{
}

template <class AnyType>
void CQuaternion<AnyType>::set( AnyType a, AnyType b, AnyType c, AnyType d )
{
	x = a;
	y = b;
	z = c;
	w = d;
}




template <class AnyType>
CQuaternion<AnyType> operator+( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 )
{
	CQuaternion<AnyType> result( q1.x + q2.x,
						q1.y + q2.y,
						q1.z + q2.z,
						q1.w + q2.w );
	return result;
}

template <class AnyType>
CQuaternion<AnyType> operator-( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 )
{
	CQuaternion<AnyType> result( q1.x - q2.x,
						q1.y - q2.y,
						q1.z - q2.z,
						q1.w - q2.w );
	return result;
}

template <class AnyType>
CQuaternion<AnyType> operator*( const CQuaternion<AnyType> &q1, const CQuaternion<AnyType> &q2 )
{
	CQuaternion<AnyType> result( q1.w*q2.x + q1.x*q2.w + q1.y*q2.z - q1.z*q2.y,
						q1.w*q2.y - q1.x*q2.z + q1.y*q2.w + q1.z*q2.x,
						q1.w*q2.z + q1.x*q2.y - q1.y*q2.x + q1.z*q2.w,
						q1.w*q2.w - q1.x*q2.x - q1.y*q2.y - q1.z*q2.z );
	return result;
}