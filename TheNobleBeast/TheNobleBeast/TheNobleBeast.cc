/*
 *	TheNobleBeast.cc
 *
 *	Coded by Joseph A. Marrero
 *	12/4/06
 */
#include <cassert>
#include <iostream>
#include "TheNobleBeast.h"
#include "glut/glut.h"
using namespace std;

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( 640, 480 );
	
	glutCreateWindow( "The Noble Beast" );
	//glutFullScreen( );
	initialize( );
	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );
	glutMainLoop( );
	return 0;
}


void initialize( )
{
	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	//glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );						
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_DEPTH_TEST );


	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	/* Build cubic splines for curves 1, 2 and 3. */
	buildCubicSplines( );
}

void deinitialize( )
{
}


void render( )
{
	glClearColor( 0.1f, 0.1f, 0.1f, 0.1f );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity( );
	
	glPointSize( 3.0f );

	drawGrid( );


	if( bGraphNaturalCubicSpline )
		graphNaturalCubicSpline( );
	if( bGraphClampedCubicSpline )
		graphClampedCubicSpline( );
	if( bPlotOriginalData )
		plotOriginalData( );

	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("The Noble Beast"), 2, 22 );
	writeText( GLUT_BITMAP_9_BY_15, std::string("Press Q to quit. Press O to toggle plot original data. Press N or C to toggle graph of natural / clamped cubic spline. "), 2, 5 );
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	//glOrtho( -1.0, X_WINDOW_SIZE + 1, -1.0, Y_WINDOW_SIZE, 0.0, 1.0 );
	glOrtho( 0.0, X_WINDOW_SIZE + 1, 0.0, Y_WINDOW_SIZE, 0.0, 1.0 );
	glMatrixMode( GL_MODELVIEW );

}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		case 'O':
		case 'o':
			bPlotOriginalData = !bPlotOriginalData;
			glutPostRedisplay( );
			break;
		case 'N':
		case 'n':
			bGraphNaturalCubicSpline = !bGraphNaturalCubicSpline;
			glutPostRedisplay( );
			break;
		case 'C':
		case 'c':
			bGraphClampedCubicSpline = !bGraphClampedCubicSpline;
			glutPostRedisplay( );
			break;
		default:
			break;
	}

}

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	glDisable( GL_DEPTH_TEST );
	glDisable(GL_TEXTURE_2D);

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, width, 0, height, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glRasterPos2i( x, y );

			for( unsigned int i = 0; i < text.size( ); i++ )
				glutBitmapCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	glEnable( GL_DEPTH_TEST );
	glEnable(GL_TEXTURE_2D);

}

void idle( )
{ glutPostRedisplay( ); }


/*
 * Helpers...
 */
void drawGrid( )
{
	glPushMatrix( );
	glPushAttrib( GL_CURRENT_BIT );
		glColor3f( 0.3f, 0.3f, 0.3f );
		glEnable( GL_LINE_STIPPLE );
		glLineStipple( 1, 0x0303 );
		glBegin( GL_LINES );
		for( int y = 0; y < Y_WINDOW_SIZE; y += 1 )
			for( int x = 0; x < X_WINDOW_SIZE; x += 1 )
			{
				glVertex2i( x + 1, y );
				glVertex2i( x, y );
			}
		glEnd( );

		glBegin( GL_LINES );
		for( int y = 0; y <= Y_WINDOW_SIZE; y += 1 )
			for( int x = 0; x <= X_WINDOW_SIZE; x += 1 )
			{
				glVertex2i( x, y - 1 );
				glVertex2i( x, y );
			}
		glEnd( );
		glDisable( GL_LINE_STIPPLE );

	glPopAttrib( );
	glPopMatrix( );
}

void plotOriginalData( )
{
	glPushMatrix( );
	glLoadIdentity( );
	glPushAttrib( GL_CURRENT_BIT );
		glColor3f( 1.0f, 0, 0 );
		glPointSize( 4.0f );

		glBegin( GL_POINTS );
			for( int i = 0; i < NUMBER_OF_NODES_CURVE1; i++ )
				glVertex2d( x_of_curve1[ i ], f_of_curve1[ i ] );
			for( int i = 0; i < NUMBER_OF_NODES_CURVE2; i++ )
				glVertex2d( x_of_curve2[ i ], f_of_curve2[ i ] );
			for( int i = 0; i < NUMBER_OF_NODES_CURVE3; i++ )
				glVertex2d( x_of_curve3[ i ], f_of_curve3[ i ] );
		glEnd( );
	glPopAttrib( );
	glPopMatrix( );
}

void buildCubicSplines( )
{
	/*
	 *	Create a natural cubic spline for each curve...
	 */
	buildNaturalCubicSpline( x_of_curve1, f_of_curve1, NUMBER_OF_NODES_CURVE1, naturalCubicSplineOfCurve );
	buildNaturalCubicSpline( x_of_curve2, f_of_curve2, NUMBER_OF_NODES_CURVE2, naturalCubicSplineOfCurve );
	buildNaturalCubicSpline( x_of_curve3, f_of_curve3, NUMBER_OF_NODES_CURVE3, naturalCubicSplineOfCurve );
	/*
	*	Create a clamped cubic spline for each curve...
	*/
	buildClampedCubicSpline( x_of_curve1, f_of_curve1, 1.0, -0.67, NUMBER_OF_NODES_CURVE1, clampedCubicSplineOfCurve );
	buildClampedCubicSpline( x_of_curve2, f_of_curve2, 3.0, -4.0, NUMBER_OF_NODES_CURVE2, clampedCubicSplineOfCurve );
	buildClampedCubicSpline( x_of_curve3, f_of_curve3, 0.33, -1.5, NUMBER_OF_NODES_CURVE3, clampedCubicSplineOfCurve );

	// Print formulas...
	printFormulas( );
}


void buildNaturalCubicSpline( double *x_domain, double *range, const int numberOfNodes, list<CubicSplinePiece> &curveList )
{
	double *h = new double[ numberOfNodes - 1 ];
	double *alpha = new double[ numberOfNodes - 1 ];
	double *l = new double[ numberOfNodes ];
	double *mu = new double[ numberOfNodes ];
	double *z = new double[ numberOfNodes ];
	double *b = new double[ numberOfNodes ];
	double *c = new double[ numberOfNodes ];
	double *d = new double[ numberOfNodes ];


	for( int i = 0; i <= numberOfNodes - 2; i++ )
		h[ i ] = x_domain[ i + 1 ] - x_domain[ i ];

	for( int i = 1; i <= numberOfNodes - 2; i++ )
		alpha[ i ] = (3 / h[ i ]) * (range[ i + 1 ] - range[ i ]) - (3 / h[ i - 1 ]) * (range[i] - range[ i - 1 ]);

	l[ 0 ] = 1.0;
	mu[ 0 ] = 0.0;
	z[ 0 ] = 0.0;

	for( int i = 1; i <= numberOfNodes - 2; i++ )
	{
		l[ i ] = 2 * ( x_domain[ i + 1 ] - x_domain[ i - 1 ] ) - h[ i - 1 ] * mu[ i - 1 ];
		mu[ i ] = h[ i ] / l[ i ];
		z[ i ] = ( alpha[ i ] - h[ i - 1 ] * z[ i - 1 ] ) / l[ i ];
	}

	l[ numberOfNodes - 1 ] = 1.0;
	z[ numberOfNodes - 1 ] = 0.0;
	c[ numberOfNodes - 1 ] = 0.0;

	for( int j = numberOfNodes - 2; j >= 0 ; j-- )
	{
		c[ j ] = z[ j ] - mu[ j ] * c[ j + 1 ];
		b[ j ] = (range[ j + 1 ] - range[ j ]) / h[ j ] - h[ j ] * ( c[ j + 1 ] + 2 * c[ j ]) / 3;
		d[ j ] = (c[ j + 1 ] - c[ j ]) / ( 3 * h[ j ] );
	}


	for( int i = 0; i < numberOfNodes; i++ )
	{
		CubicSplinePiece piece;
		memset( &piece, 0, sizeof(CubicSplinePiece) );
		piece.ai = range[ i ];
		piece.bi = b[ i ];
		piece.ci = c[ i ];
		piece.di = d[ i ];
		piece.xi = x_domain[ i ];

		piece.domain_start = x_domain[ i ];
		piece.domain_end = x_domain[ i + 1 ];
		curveList.push_back( piece );
	}

	delete [] h;
	delete [] alpha;
	delete [] l;
	delete [] mu;
	delete [] z;
	delete [] b;
	delete [] c;
	delete [] d;
}

void buildClampedCubicSpline( double *x_domain, double *range, double f_prime_at_x0, double f_prime_at_xn,
							 const int numberOfNodes, list<CubicSplinePiece> &curveList )
{
	double *h = new double[ numberOfNodes - 1 ];
	double *alpha = new double[ numberOfNodes ];
	double *l = new double[ numberOfNodes ];
	double *mu = new double[ numberOfNodes ];
	double *z = new double[ numberOfNodes ];
	double *b = new double[ numberOfNodes ];
	double *c = new double[ numberOfNodes ];
	double *d = new double[ numberOfNodes ];


	for( int i = 0; i <= numberOfNodes - 2; i++ )
		h[ i ] = x_domain[ i + 1 ] - x_domain[ i ];

	alpha[ 0 ] = 3 * (range[ 1 ] - range[ 0 ]) / h[ 0 ] - 3 * f_prime_at_x0;
	alpha[ numberOfNodes - 1 ] = 3 * f_prime_at_xn - 3 * (range[ numberOfNodes - 1 ] - range[ numberOfNodes - 2 ]) / h[ numberOfNodes - 2 ];

	for( int i = 1; i <= numberOfNodes - 2; i++ )
		alpha[ i ] = (3 / h[ i ]) * (range[ i + 1 ] - range[ i ]) - (3 / h[ i - 1 ]) * (range[i] - range[ i - 1 ]);


	l[ 0 ] = 2 * h[ 0 ];
	mu[ 0 ] = 0.5;
	z[ 0 ] = alpha[ 0 ] / l[ 0 ];

	for( int i = 1; i <= numberOfNodes - 2; i++ )
	{
		l[ i ] = 2 * ( x_domain[ i + 1 ] - x_domain[ i - 1 ] ) - h[ i - 1 ] * mu[ i - 1 ];
		mu[ i ] = h[ i ] / l[ i ];
		z[ i ] = ( alpha[ i ] - h[ i - 1 ] * z[ i - 1 ] ) / l[ i ];
	}

	l[ numberOfNodes - 1 ] = h[ numberOfNodes - 2 ] * (2 - mu[ numberOfNodes - 2 ]);
	z[ numberOfNodes - 1 ] = (alpha[ numberOfNodes - 1 ] - h[ numberOfNodes - 2 ] * z[ numberOfNodes - 2 ]) / l[ numberOfNodes - 1 ];
	c[ numberOfNodes - 1 ] = z[ numberOfNodes - 1 ];

	for( int j = numberOfNodes - 2; j >= 0 ; j-- )
	{
		c[ j ] = z[ j ] - mu[ j ] * c[ j + 1 ];
		b[ j ] = (range[ j + 1 ] - range[ j ]) / h[ j ] - h[ j ] * ( c[ j + 1 ] + 2 * c[ j ]) / 3;
		d[ j ] = (c[ j + 1 ] - c[ j ]) / ( 3 * h[ j ] );
	}

	for( int i = 0; i < numberOfNodes; i++ )
	{
		CubicSplinePiece piece;
		memset( &piece, 0, sizeof(CubicSplinePiece) );
		piece.ai = range[ i ];
		piece.bi = b[ i ];
		piece.ci = c[ i ];
		piece.di = d[ i ];
		piece.xi = x_domain[ i ];

		piece.domain_start = x_domain[ i ];
		piece.domain_end = x_domain[ i + 1 ];
		curveList.push_back( piece );
	}

	delete [] h;
	delete [] alpha;
	delete [] l;
	delete [] mu;
	delete [] z;
	delete [] b;
	delete [] c;
	delete [] d;
}


void graphNaturalCubicSpline( )
{
	list<CubicSplinePiece>::iterator itr;

	glPushMatrix( );
		glPushAttrib( GL_CURRENT_BIT );
		glColor3f( 0.0f, 0.0f, 1.0f );
				
		for( itr = naturalCubicSplineOfCurve.begin( ); itr != naturalCubicSplineOfCurve.end( ); ++itr )
		{
			double ai = itr->ai;
			double bi = itr->bi;
			double ci = itr->ci;
			double di = itr->di;
			double xi = itr->xi;

			double domain_start = itr->domain_start;
			double domain_end = itr->domain_end;

			glBegin( GL_LINE_STRIP );
			for( double i = itr->domain_start; i < itr->domain_end; i += 0.08 )
				glVertex2d( i, Si( (*itr), i ) );  
			glEnd( );
		}

		glPopAttrib( );
	glPopMatrix( );

}

void graphClampedCubicSpline( )
{
	list<CubicSplinePiece>::iterator itr;

	glPushMatrix( );
		glPushAttrib( GL_CURRENT_BIT );
		glColor3f( 0.0f, 1.0f, 0.0f );

		for( itr = clampedCubicSplineOfCurve.begin( ); itr != clampedCubicSplineOfCurve.end( ); ++itr )
		{
			double ai = itr->ai;
			double bi = itr->bi;
			double ci = itr->ci;
			double di = itr->di;
			double xi = itr->xi;

			double domain_start = itr->domain_start;
			double domain_end = itr->domain_end;

			glBegin( GL_LINE_STRIP );
			for( double i = itr->domain_start; i < itr->domain_end; i += 0.08 )
				glVertex2d( i, Si( (*itr), i ) ); 
			glEnd( );
		}

		glPopAttrib( );
	glPopMatrix( );
}

void printFormulas( )
{
	list<CubicSplinePiece>::iterator itr;
	unsigned int i = 0;
	cout.precision( 2 );
	cout << showpoint;
	//cout.setf( ios::scientific );

	cout << "Explicit Formula for the Natural Cubic Spline" << endl;
	cout << "==============================================" << endl << endl;

	for( itr = naturalCubicSplineOfCurve.begin( ); itr != naturalCubicSplineOfCurve.end( ); ++itr )
	{
		CubicSplinePiece p = *itr;
		cout.width( 3 );
		cout << "S_" << i++ << "( x ) = ";
		cout.width( 3 );
		cout << p.ai << " + ";
		cout.width( 3 );
		cout << p.bi << "(x - ";
		cout.width( 3 );
		cout << p.xi << ") + ";
		cout.width( 3 );
		cout << p.ci << "(x - ";
		cout.width( 3 );
		cout << p.xi << ")^2 + ";
		cout.width( 3 );
		cout << p.di << "(x - ";
		cout.width( 3 );
		cout << p.xi << ")^3  on [";
		cout.width( 3 );
		cout << p.domain_start << ", ";
		cout.width( 3 );
		cout << p.domain_end << "]";
		cout.width( 3 );
		cout << endl;
	}
	
	cout << endl;

	i = 0;
	cout << "Explicit Formula for the Clamped Cubic Spline" << endl;
	cout << "==============================================" << endl << endl;

	for( itr = clampedCubicSplineOfCurve.begin( ); itr != clampedCubicSplineOfCurve.end( ); ++itr )
	{
		CubicSplinePiece p = *itr;
		cout.width( 3 );
		cout << "S_" << i++ << "( x ) = ";
		cout.width( 3 );
		cout << p.ai << " + ";
		cout.width( 3 );
		cout << p.bi << "(x - ";
		cout.width( 3 );
		cout << p.xi << ") + ";
		cout.width( 3 );
		cout << p.ci << "(x - ";
		cout.width( 3 );
		cout << p.xi << ")^2 + ";
		cout.width( 3 );
		cout << p.di << "(x - ";
		cout.width( 3 );
		cout << p.xi << ")^3  on [";
		cout.width( 3 );
		cout << p.domain_start << ", ";
		cout.width( 3 );
		cout << p.domain_end << "]";
		cout.width( 3 );
		cout << endl;
	}
}
