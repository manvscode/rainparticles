#ifndef _THENOBLEBEAST_H_
#define _THENOBLEBEAST_H_
/*
 *	TheNobleBeast.h
 *
 *	Coded by Joseph A. Marrero
 *	12/4/06
 */
#include <string>
#include <cmath>
#include <list>
using namespace std;

#define X_WINDOW_SIZE	30
#define Y_WINDOW_SIZE	20

void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void writeText( void *font, std::string &text, int x, int y );
void idle( );


/*
*	Cubic Spline Polynomial
*/
typedef struct tagCubicSplinePiece {
	double ai;
	double bi;
	double ci;
	double di;
	double xi;

	double domain_start;
	double domain_end;
} CubicSplinePiece;


/* helpers */
void drawGrid( );
void plotOriginalData( );
void buildCubicSplines( );
void buildNaturalCubicSpline( double *x_domain, double *range, const int numberOfNodes, list<CubicSplinePiece> &curveList );
void buildClampedCubicSpline( double *x_domain, double *range, double f_prime_at_x0, double f_prime_at_xn,
							  const int numberOfNodes, list<CubicSplinePiece> &curveList );

void graphNaturalCubicSpline( );
void graphClampedCubicSpline( );
void printFormulas( );


/* Program State */
bool bPlotOriginalData = true;
bool bGraphNaturalCubicSpline = true;
bool bGraphClampedCubicSpline = true;






#define Si( cs, x )			( cs.ai + cs.bi * ((x) - cs.xi) + cs.ci * pow((x) - cs.xi, 2) + cs.di * pow((x) - cs.xi, 3) )
#define Si_prime( cs, x )	( cs.bi + 2 * ci * ((x) - xi)) + 3 * di * pow((x) - xi, 2) )


/*
 *	Curve 1
 */
#define NUMBER_OF_NODES_CURVE1	9

double x_of_curve1[ NUMBER_OF_NODES_CURVE1 ] = {
	1,
	2,
	5,
	6,
	7,
	8,
	10,
	13,
	17
};

double f_of_curve1[ NUMBER_OF_NODES_CURVE1 ] = { 
	3.0,
	3.7,
	3.9,
	4.2, 
	5.7, 
	6.6, 
	7.1, 
	6.7, 
	4.5
};


/*
*	Curve 2
*/
#define NUMBER_OF_NODES_CURVE2	7

double x_of_curve2[ NUMBER_OF_NODES_CURVE2 ] = {
	17,
	20,
	23,
	24,
	25,
	27,
	27.7
};

double f_of_curve2[ NUMBER_OF_NODES_CURVE2 ] = { 
	4.5,
	7.0,
	6.1,
	5.6, 
	5.8, 
	5.2, 
	4.1
};

/*
*	Curve 3
*/
#define NUMBER_OF_NODES_CURVE3	4

double x_of_curve3[ NUMBER_OF_NODES_CURVE3 ] = {
	27.7,
	28,
	29,
	30
};

double f_of_curve3[ NUMBER_OF_NODES_CURVE3 ] = { 
	4.1, 
	4.3, 
	4.1, 
	3.0 
};


/*
 *	Linked list of Cubic spline pieces (polynomials)
 */

list<CubicSplinePiece> naturalCubicSplineOfCurve;
list<CubicSplinePiece> clampedCubicSplineOfCurve;

#endif
