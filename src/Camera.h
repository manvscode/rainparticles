#ifndef __CAMERA_H__
#define __CAMERA_H__
#include "Frustum.h"


typedef struct tagCamera {
	union {
		struct {
			float xPos, yPos, zPos;
		};
		float position[ 3 ];
	};
	
	union {
		struct {
			float xAngle, yAngle, zAngle;
		};
		float angles[ 3 ];
	};

	//Vector3 direction;
} Camera;

extern Camera theCamera;


extern CFrustum frustum;

#endif