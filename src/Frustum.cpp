//#include <GL/GL.h>
#include <GL/glut.h>
#include <math.h>
#include "Frustum.h"

CFrustum::CFrustum( )
{
}

CFrustum::~CFrustum( )
{
}

void CFrustum::calculateFrustum( bool normalize )
{
	float projection[ 16 ];
	float modelview[ 16 ];
	float clip[ 16 ];

	glGetFloatv( GL_PROJECTION_MATRIX, projection );
	glGetFloatv( GL_MODELVIEW_MATRIX, modelview );

	// Multiply the project and the modelview matrices together
	clip[ 0 ] = modelview[ 0 ] * projection[ 0 ] + modelview[ 1 ] * projection[ 4 ] + modelview[ 2 ] * projection[ 8 ] + modelview[ 3 ] * projection[ 12 ];
	clip[ 1 ] = modelview[ 0 ] * projection[ 1 ] + modelview[ 1 ] * projection[ 5 ] + modelview[ 2 ] * projection[ 9 ] + modelview[ 3 ] * projection[ 13 ];
	clip[ 2 ] = modelview[ 0 ] * projection[ 2 ] + modelview[ 1 ] * projection[ 6 ] + modelview[ 2 ] * projection[ 10 ] + modelview[ 3 ] * projection[ 14 ];
	clip[ 3 ] = modelview[ 0 ] * projection[ 3 ] + modelview[ 1 ] * projection[ 7 ] + modelview[ 2 ] * projection[ 11 ] + modelview[ 3 ] * projection[ 15 ];

	clip[ 4 ] = modelview[ 4 ] * projection[ 0 ] + modelview[ 5 ] * projection[ 4 ] + modelview[ 6 ] * projection[ 8 ] + modelview[ 7 ] * projection[ 12 ];
	clip[ 5 ] = modelview[ 4 ] * projection[ 1 ] + modelview[ 5 ] * projection[ 5 ] + modelview[ 6 ] * projection[ 9 ] + modelview[ 7 ] * projection[ 13 ];
	clip[ 6 ] = modelview[ 4 ] * projection[ 2 ] + modelview[ 5 ] * projection[ 6 ] + modelview[ 6 ] * projection[ 10 ] + modelview[ 7 ] * projection[ 14 ];
	clip[ 7 ] = modelview[ 4 ] * projection[ 3 ] + modelview[ 5 ] * projection[ 7 ] + modelview[ 6 ] * projection[ 11 ] + modelview[ 7 ] * projection[ 15 ];

	clip[ 8 ] = modelview[ 8 ] * projection[ 0 ] + modelview[ 9 ] * projection[ 4 ] + modelview[ 10 ] * projection[ 8 ] + modelview[ 11 ] * projection[ 12 ];
	clip[ 9] = modelview[ 8 ] * projection[ 1 ] + modelview[ 9 ] * projection[ 5 ] + modelview[ 10 ] * projection[ 9 ] + modelview[ 11 ] * projection[ 13 ];
	clip[ 10 ] = modelview[ 8 ] * projection[ 2 ] + modelview[ 9 ] * projection[ 6 ] + modelview[ 10 ] * projection[ 10 ] + modelview[ 11 ] * projection[ 14 ];
	clip[ 11 ] = modelview[ 8 ] * projection[ 3 ] + modelview[ 9 ] * projection[ 7 ] + modelview[ 10 ] * projection[ 11 ] + modelview[ 11 ] * projection[ 15 ];

	clip[ 12 ] = modelview[ 12 ] * projection[ 0 ] + modelview[ 13 ] * projection[ 4 ] + modelview[ 14 ] * projection[ 8 ] + modelview[ 15 ] * projection[ 12 ];
	clip[ 13 ] = modelview[ 12 ] * projection[ 1 ] + modelview[ 13 ] * projection[ 5 ] + modelview[ 14 ] * projection[ 9 ] + modelview[ 15 ] * projection[ 13 ];
	clip[ 14 ] = modelview[ 12 ] * projection[ 2 ] + modelview[ 13 ] * projection[ 6 ] + modelview[ 14 ] * projection[ 10 ] + modelview[ 15 ] * projection[14 ];
	clip[ 15 ] = modelview[ 12 ] * projection[ 3 ] + modelview[ 13 ] * projection[ 7 ] + modelview[ 14 ] * projection[ 11 ] + modelview[ 15 ] * projection[ 15 ];

	// Store the Frustum's planes
	m_Frustum[ RIGHT ][ A ] = clip[ 3 ] - clip[ 0 ];
	m_Frustum[ RIGHT ][ B ] = clip[ 7 ] - clip[ 4 ];
	m_Frustum[ RIGHT ][ C ] = clip[ 11 ] - clip[ 8 ];
	m_Frustum[ RIGHT ][ D ] = clip[ 15 ] - clip[ 12 ];

	m_Frustum[ LEFT ][ A ] = clip[ 3 ] + clip[ 0 ];
	m_Frustum[ LEFT ][ B ] = clip[ 7 ] + clip[ 4 ];
	m_Frustum[ LEFT ][ C ] = clip[ 11 ] + clip[ 8 ];
	m_Frustum[ LEFT ][ D ] = clip[ 15 ] + clip[12 ];

	m_Frustum[ BOTTOM ][ A ] = clip[ 3 ] + clip[ 1 ];
	m_Frustum[ BOTTOM ][ B ] = clip[ 7 ] + clip[ 5 ];
	m_Frustum[ BOTTOM ][ C ] = clip[ 11 ] + clip[ 9 ];
	m_Frustum[ BOTTOM ][ D ] = clip[ 15 ] + clip[ 13 ];

	m_Frustum[ TOP ][ A ] = clip[ 3 ] - clip[ 1 ];
	m_Frustum[ TOP ][ B ] = clip[ 7 ] - clip[ 5 ];
	m_Frustum[ TOP ][ C ] = clip[ 11 ] - clip[ 9 ];
	m_Frustum[ TOP ][ D ] = clip[ 15 ] - clip[ 13 ];

	m_Frustum[ BACK ][ A ] = clip[ 3 ] - clip[ 2 ];
	m_Frustum[ BACK ][ B ] = clip[ 7 ] - clip[ 6 ];
	m_Frustum[ BACK ][ C ] = clip[ 11 ] - clip[ 10 ];
	m_Frustum[ BACK ][ D ] = clip[ 15 ] - clip[ 14 ];

	m_Frustum[ FRONT ][ A ] = clip[ 3] + clip[ 2];
	m_Frustum[ FRONT ][ B ] = clip[ 7] + clip[ 6];
	m_Frustum[ FRONT ][ C ] = clip[11] + clip[10];
	m_Frustum[ FRONT ][ D ] = clip[15] + clip[14];


	if( normalize )
	{
		CFrustum::normalize( RIGHT );
		CFrustum::normalize( LEFT );
		CFrustum::normalize( BOTTOM );
		CFrustum::normalize( TOP );
		CFrustum::normalize( BACK );
		CFrustum::normalize( FRONT );
	}
}

bool CFrustum::isPointInFrustum( float x, float y, float z )
{
	// For each plane in the frustum...
	for( unsigned int i = 0; i < 6; i++ )
	{
		// check if the dot product with the plane's normal is <= 0  (object is behind this plane)
		if( m_Frustum[ i ][ A ] * x + m_Frustum[ i ][ B ] * y + m_Frustum[ i ][ C ] * z + m_Frustum[ i ][ D ] <= 0 )
			return false;
	}

	return true;
}

bool CFrustum::isSphereInFrustum( float x, float y, float z, float radius )
{
	// For each plane in the frustum...
	for( unsigned int i = 0; i < 6; i++ )	
	{
		// the normal vector dotted with a point on the plane is -D
		if( m_Frustum[ i ][ A ] * x + m_Frustum[ i ][ B ] * y + m_Frustum[ i ][ C ] * z + m_Frustum[ i ][ D ] <= -radius )
			return false;
	}

	return true;
}

bool CFrustum::isCubeInFrustum( float x, float y, float z, float size )
{
	for( unsigned int i = 0; i < 6; i++ )
	{
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z - size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y - size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x - size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
			continue;
		if(m_Frustum[i][A] * (x + size) + m_Frustum[i][B] * (y + size) + m_Frustum[i][C] * (z + size) + m_Frustum[i][D] > 0)
			continue;

		// If we get here, it isn't in the frustum
		return false;
	}

	return true;
}

void CFrustum::normalize( Plane p )
{
	float d = sqrt( m_Frustum[ p ][ 0 ] * m_Frustum[ p ][ 0 ] +
						 m_Frustum[ p ][ 1 ] * m_Frustum[ p ][ 1 ] +
						 m_Frustum[ p ][ 2 ] * m_Frustum[ p ][ 2 ] +
						 m_Frustum[ p ][ 3 ] * m_Frustum[ p ][ 3 ] );

	m_Frustum[ p ][ 0 ] /= d;
	m_Frustum[ p ][ 1 ] /= d;
	m_Frustum[ p ][ 2 ] /= d;
	m_Frustum[ p ][ 3 ] /= d;
}