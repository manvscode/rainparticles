#pragma once
#include "3DPoint.h"

template <class AnyType = float>
class C3DBox
{
  public:
	explicit C3DBox( )
	{
		m_Width = static_cast<AnyType>( 0 );
		m_Length = static_cast<AnyType>( 0 );
		m_Height = static_cast<AnyType>( 0 );

		m_Points[ 0 ] = C3DPoint<AnyType>( );
		m_Points[ 1 ] = C3DPoint<AnyType>( );
		m_Points[ 2 ] = C3DPoint<AnyType>( );
		m_Points[ 3 ] = C3DPoint<AnyType>( );
		m_Points[ 4 ] = C3DPoint<AnyType>( );
		m_Points[ 5 ] = C3DPoint<AnyType>( );
		m_Points[ 6 ] = C3DPoint<AnyType>( );
		m_Points[ 7 ] = C3DPoint<AnyType>( );
	}

	explicit C3DBox( const C3DPoint<AnyType> &p1, const C3DPoint<AnyType> &p2 )
	{
		m_Width = abs( p2.getX( ) - p1.getX( ) );
		m_Length = abs( p2.getZ( ) - p1.getZ( ) );
		m_Height = abs( p2.getY( ) - p1.getY( ) );

		m_Points[ 0 ].set( p1.getX( ), p1.getY( ), p1.getZ( ) );
		m_Points[ 1 ].set( p1.getX( ) + m_Width, p1.getY( ), p1.getZ( ) );
		m_Points[ 2 ].set( p1.getX( ) + m_Width, p1.getY( ), p1.getZ( ) + m_Length );
		m_Points[ 3 ].set( p1.getX( ), p1.getY( ), p1.getZ( ) + m_Length );
		m_Points[ 4 ].set( p2.getX( ) - m_Width, p2.getY( ), p2.getZ( ) - m_Length );
		m_Points[ 5 ].set( p2.getX( ), p2.getY( ), p2.getZ( ) - m_Length );
		m_Points[ 6 ].set( p2.getX( ), p2.getY( ), p2.getZ( ) );
		m_Points[ 7 ].set( p2.getX( ) - m_Width, p2.getY( ), p2.getZ( ) );
	}

	explicit C3DBox( const C3DPoint<AnyType> &center, const AnyType width, const AnyType height, const AnyType length )
	{
		float halfWidth = width / 2.0f;
		float halfHeight = height / 2.0f;
		float halfLength = length / 2.0f;

		m_Width = width;
		m_Length = length;
		m_Height = height;

		m_Points[ 0 ].set( center.getX( ) - halfWidth, center.getY( ) - halfHeight, center.getZ( ) - halfLength );
		m_Points[ 1 ].set( center.getX( ) + halfWidth, center.getY( ) - halfHeight, center.getZ( ) - halfLength );
		m_Points[ 2 ].set( center.getX( ) + halfWidth, center.getY( ) - halfHeight, center.getZ( ) + halfLength );
		m_Points[ 3 ].set( center.getX( ) - halfWidth, center.getY( ) - halfHeight, center.getZ( ) + halfLength );
		m_Points[ 4 ].set( center.getX( ) - halfWidth, center.getY( ) + halfHeight, center.getZ( ) - halfLength );
		m_Points[ 5 ].set( center.getX( ) + halfWidth, center.getY( ) + halfHeight, center.getZ( ) - halfLength );
		m_Points[ 6 ].set( center.getX( ) + halfWidth, center.getY( ) + halfHeight, center.getZ( ) + halfLength );
		m_Points[ 7 ].set( center.getX( ) - halfWidth, center.getY( ) + halfHeight, center.getZ( ) + halfLength );
	}

	virtual ~C3DBox( void )
	{
	}

	AnyType getWidth( ) const
	{ return m_Width; }

	AnyType getLength( ) const
	{ return m_Length; }
	
	AnyType getHeight( ) const
	{ return m_Height; }


  protected:
	AnyType m_Width;
	AnyType m_Length;
	AnyType m_Height;
	C3DPoint<AnyType> m_CenterPoint;
	C3DPoint<AnyType> m_Points[ 8 ];
};
