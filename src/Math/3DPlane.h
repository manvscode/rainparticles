#pragma once
#ifndef __3DPLANE_H__
#define __3DPLANE_H__

#include "3DPoint.h"

template <class AnyType>
class C3DPlane
{
  public:

	explicit C3DPlane(void)
	{}
	explicit C3DPlane( const C3DPoint<AnyType> &p1, const C3DPoint<AnyType> &p2, const C3DPoint<AnyType> &p3 )
	{
		Vector3 v1( p2 - p1 );
		Vector3 v2( p3 - p2 );
		vNormal = CrossProduct( v1, v2 ); //v1 >< v2;
		pt = p1;
	}
	explicit C3DPlane( const C3DPoint<AnyType> &p, const Vector3<AnyType> &normal )
	{
		pt = p;
		vNormal = normal;
	}


	virtual ~C3DPlane(void)
	{
	}
	


  private:
	C3DPoint pt;
	Vector3 vNormal;
};

#endif