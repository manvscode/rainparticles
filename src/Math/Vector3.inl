/*		Vector3.cpp
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */
#include <cmath>

template <class AnyType>
Vector3<AnyType>::Vector3(void)
{
	x = y = z = 0.0;

}
template <class AnyType>
Vector3<AnyType>::Vector3(const AnyType x, const AnyType y, const AnyType z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}
template <class AnyType>
Vector3<AnyType>::~Vector3(void)
{
}
template <class AnyType>
const AnyType Vector3<AnyType>::getX( ) const
{
	return x;
}
template <class AnyType>
const AnyType Vector3<AnyType>::getY( ) const
{
	return y;
}
template <class AnyType>
const AnyType Vector3<AnyType>::getZ( ) const
{
	return z;
}
template <class AnyType>
void Vector3<AnyType>::setX( const AnyType X )
{ x = X; }
template <class AnyType>
void Vector3<AnyType>::setY( const AnyType Y )
{ y = Y; }
template <class AnyType>
void Vector3<AnyType>::setZ( const AnyType Z )
{ z = Z; }
template <class AnyType>
const Vector3<AnyType> &Vector3<AnyType>::operator =(const Vector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = vector.getX( );
		y = vector.getY( );
		z = vector.getZ( );
	}
	return *this;
}
template <class AnyType>
const Vector3<AnyType> &Vector3<AnyType>::operator +=(const Vector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x + vector.getX( );
		y = y + vector.getY( );
		z = z + vector.getZ( );
	}

	return *this;
}
template <class AnyType>
const Vector3<AnyType> &Vector3<AnyType>::operator -=(const Vector3<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x - vector.getX( );
		y = y - vector.getY( );
		z = z - vector.getZ( );
	}
    
    return *this;	
}
template <class AnyType>
AnyType Vector3<AnyType>::dotProduct(const Vector3<AnyType> &vector)
{
	return (x * vector.x + y * vector.y + z * vector.z);
}
template <class AnyType>
Vector3<AnyType> Vector3<AnyType>::crossProduct(const Vector3<AnyType> &vector)
{
	Vector3<AnyType> Result;

	Result.x = y*vector.z - z*vector.y;
	Result.y = z*vector.x - x*vector.z;
	Result.z = x*vector.y - y*vector.x;

	return Result;
}
//ostream& operator << (ostream& out, const Vector3<AnyType> &vector)
//{
//	out << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
//
//	return out;
//}
template <class AnyType>
AnyType Vector3<AnyType>::getMagnitude( ) const
{
	return sqrt( x*x + y*y + z*z );
}

template <class AnyType>
void Vector3<AnyType>::normalize( )
{
	AnyType m = getMagnitude( );
	x = x / m;
	y = y / m;
	z = z / m;
	//*this/getMagnitude();
}

template <class AnyType>
const bool Vector3<AnyType>::isNormalized( ) const
{ return ( x == 1 && y == 1 && z == 1 ); }

template <class AnyType>
void Vector3<AnyType>::negate()
{
	x = -x;
	y = -y;
	z = -z;
}


// static common vectors
template <class AnyType>
const Vector3<AnyType> Vector3<AnyType>::X_VECTOR( static_cast<AnyType>(1),  static_cast<AnyType>(0), static_cast<AnyType>(0) );

template <class AnyType>
const Vector3<AnyType> Vector3<AnyType>::Y_VECTOR( static_cast<AnyType>(0),  static_cast<AnyType>(1), static_cast<AnyType>(0) );

template <class AnyType>
const Vector3<AnyType> Vector3<AnyType>::Z_VECTOR( static_cast<AnyType>(0), static_cast<AnyType>(0),  static_cast<AnyType>(1) );


template <class AnyType>
Vector3<AnyType> operator +(const Vector3<AnyType> &v1, const Vector3<AnyType> &v2)				// Vector Sum
{
	Vector3<AnyType> Result;
	
	Result.setX( v1.getX( ) + v2.getX( ) );
	Result.setY( v1.getY( ) + v2.getY( ) );
	Result.setZ( v1.getZ( ) + v2.getZ( ) );

	return Result;
}
template <class AnyType>
Vector3<AnyType> operator -(const Vector3<AnyType> &v1, const Vector3<AnyType> &v2)				// Vector Difference
{
	Vector3<AnyType> Result;
	
	Result.setX( v1.getX( ) - v2.getX( ) );
	Result.setY( v1.getY( ) - v2.getY( ) );
	Result.setZ( v1.getZ( ) - v2.getZ( ) );

	return Result;
}
template <class AnyType>
Vector3<AnyType> operator *(const Vector3<AnyType> &vector, const AnyType scalar)			// Vector-Scalar Multiplication
{
	Vector3<AnyType> Result;

	Result.setX( scalar * vector.getX( ) );
	Result.setY( scalar * vector.getY( ) );
	Result.setZ( scalar * vector.getZ( ) );

	return Result;
}
template <class AnyType>
Vector3<AnyType> operator *( const AnyType scalar, const Vector3<AnyType> &vector)			// Vector-Scalar Multiplication
{
	Vector3<AnyType> Result;

	Result.setX( scalar * vector.getX( ) );
	Result.setY( scalar * vector.getY( ) );
	Result.setZ( scalar * vector.getZ( ) );

	return Result;
}
template <class AnyType>
Vector3<AnyType> operator /(const Vector3<AnyType> &vector, const AnyType scalar)			// Vector- Scalar Division
{
	Vector3<AnyType> Result;

	Result.setX( vector.getX( ) / scalar );
	Result.setY( vector.getY( ) / scalar );
	Result.setZ( vector.getZ( ) / scalar );

	return Result;
}
template <class AnyType>
Vector3<AnyType> operator /(const AnyType scalar, const Vector3<AnyType> &vector)			// Vector- Scalar Division
{
	Vector3<AnyType> Result;

	Result.setX( scalar / vector.getX( ) );
	Result.setY( scalar / vector.getY( ) );
	Result.setZ( scalar / vector.getZ( ) );

	return Result;
}


template <class AnyType>
AnyType dotProduct( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 )
{
	return ( v1.getX( ) * v2.getX( ) + v1.getY( ) * v2.getY( ) + v1.getZ( ) * v2.getZ( ) );
}
template <class AnyType>
AnyType operator *( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 )
{
	return DotProduct( v1, v2 );
}
template <class AnyType>
Vector3<AnyType> crossProduct( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 )
{
	Vector3<AnyType> Result;

	Result.setX( v1.getY( ) * v2.getZ( ) - v1.getZ( ) * v2.getY( ) );
	Result.setY( v1.getZ( ) * v2.getX( ) - v1.getX( ) * v2.getZ( ) );
	Result.setZ( v1.getX( ) * v2.getY( ) - v1.getY( ) * v2.getX( ) );
	
	return Result;
}
//template <class AnyType>
//Vector3<AnyType> operator( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 )
//{
//	return CrossProduct( v1, v2 );
//}


template <class AnyType>
AnyType angleBetween( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 ) // in radians
{
	float dp = dotProduct( v1, v2 );
	float v1M = v1.getMagnitude( ),
		  v2M = v2.getMagnitude( );

	return ( (AnyType) acos( dp / ( v1M * v2M ) ) );
}
