#pragma once
#ifndef __PARTICLE_H__
#define __PARTICLE_H__
/*
 *		Particle.h
 *
 */
#include "../Math/3DPoint.h"
#include "../Math/Vector3.h"

typedef struct tagColor {
	float m_Red,
		  m_Green,
		  m_Blue;
} Color;

typedef struct tagSize3D {
	float radius;
} Size3D;

typedef struct tagParticle {
	C3DPoint<float> m_ptPosition;
	Vector3<float> m_vVelocity;
	Color m_Color;
	float m_Size;
	int m_nLifeSpan;
} Particle;

#endif