/*
 *	Rain.cpp
 *
 *
 */


#include "../TimerMain.h"
#include "../opengl.h"
#include "../Math/3DPoint.h"
#include "../Math/Matrix4x4.h"
#include "Rain.h"
#include "../Camera.h"



CRain::CRain(void)
: m_AmountOfRain( 0.5 ), pt1( 5.0f, 20.0f, 0.0f ), pt2( -5.0f, 20.0f, 0.0f )
{
	m_NumberOfRainParticles = (unsigned int) ( m_AmountOfRain * MAX_RAIN );
	initialize( );

	m_RainParticles.Emits( m_NumberOfRainParticles/*NUMBER_OF_RAINPARTICLES*/,  C3DPoint<float>( 0, 65, 0 ) );  // for testing
	//m_RainParticles.Emits( NUMBER_OF_RAINPARTICLES, C3DLine<float>( pt1, pt2 ) );
	timer.ResetTimer( );

	//srand( (unsigned int) timer.GetTime( ) );
}


CRain::CRain(const float x, const float y, const float z, Vector3<float> windVelocity, double amountOfRain )
: center(x,y,z), m_AmountOfRain( amountOfRain ), pt1( 0.0f, 14.0f, 0.0f ), pt2( 0.0f, -14.0f, 0.0f )
{
	m_NumberOfRainParticles = (unsigned int) ( m_AmountOfRain * MAX_RAIN );
	initialize( );
	m_WindVelocity = windVelocity;
	m_RainParticles.Emits( m_NumberOfRainParticles/*NUMBER_OF_RAINPARTICLES*/,  C3DPoint<float>( 0, 65, 0 ) );  // for testing
	//m_RainParticles.Emits( NUMBER_OF_RAINPARTICLES, C3DLine<float>( pt1, pt2 ) );
	timer.ResetTimer( );


	//Vector3<float> yUnit( 0.0f, 1.0f, 0.0f );
	const Vector3<float> &yUnit = Vector3<float>::Y_VECTOR;
	Vector3<float> yzWind( 0.00f, m_WindVelocity.getY( ), m_WindVelocity.getZ( ) );
	xTheta = 57.295f * angleBetween( yUnit, yzWind );

	//Vector3<float> zUnit( 0.0f, 0.0f, 1.0f );
	const Vector3<float> &zUnit = Vector3<float>::Z_VECTOR;
	Vector3<float> xzWind( m_WindVelocity.getX( ), 0.0f, m_WindVelocity.getZ( ) );
	yTheta = 57.295f * angleBetween( zUnit, xzWind );

	//Vector3<float> xyWind( 2.0f, -1.0f, 0.0f );
	Vector3<float> xyWind( m_WindVelocity.getX( ), m_WindVelocity.getY( ), 0.0f );
	zTheta = 57.295f * angleBetween( yUnit, xyWind );


	
}

const unsigned int CRain::MAX_RAIN = 256;

Vector3<float> CRain::m_WindVelocity;
CRain::~CRain(void)
{
}
void CRain::initialize( )
{
	//m_RainDrop = glGenLists( 1 );
	//
	//glNewList( m_RainDrop, GL_COMPILE );
	//	glBegin( GL_LINES );
	//		glColor3f( 0.05f, 0.05f, 0.4f );

	//		glBegin( GL_TRIANGLE_STRIP );
	//		glColor4f( 0.2f, 0.2f, 0.3f, 1.0f);
	//		x = particleArray[ i ].m_ptPosition.getX( );
	//		y = particleArray[ i ].m_ptPosition.getY( );
	//		z = particleArray[ i ].m_ptPosition.getZ( );
	//		fSize = particleArray[ i ].m_Size;


	//		glTexCoord2d( 1, 1 ); glVertex3f( x + fSize, y + fSize, z );		// Top Right
	//		glTexCoord2d( 0, 1 ); glVertex3f( x - fSize, y + fSize, z );		// Top Left
	//		glTexCoord2d( 1, 0 ); glVertex3f( x + fSize, y - 5 * fSize, z );	// Bottom Right
	//		glTexCoord2d( 0, 0 ); glVertex3f( x - fSize, y - 5 * fSize, z );	// Bottom Left
	//		glEnd( );

	//	glEnd( );
	//glEndList( );
}

void CRain::Draw( )
{
	const Particle *particleArray = m_RainParticles.getParticles( );
	size_t nNbrOfParticles = m_RainParticles.getParticlesCount( );
	float x = 0.0f, 
		  y = 0.0f,
		  z = 0.0f,
		  fSize = 0.0f;
	glEnable( GL_BLEND );
	glEnable( GL_ALPHA_TEST );
	glDisable(GL_DEPTH_TEST);
	glEnable( GL_TEXTURE_2D );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); 
	glDisable( GL_CULL_FACE );
	glBindTexture( GL_TEXTURE_2D, 0 );

	glPushMatrix( );
		//glTranslatef( center.getX( ) + WORLD_DIMENSION / 2.0f, center.getY( ), center.getZ( ) + WORLD_DIMENSION / 2.0f );


		GLfloat matrix[ 16 ] = {0};
		glGetFloatv( GL_MODELVIEW_MATRIX , matrix );

		matrix[ 0 ] = matrix[ 10 ] = 1.0f;
		matrix[ 1 ] = matrix[ 2 ] = matrix[ 8 ] = matrix[ 9 ] = 0.0f;
		matrix[12] = matrix[13] = matrix[14] = 0.0f;

		glLoadMatrixf( matrix );

		
		glTranslatef( center.getX( ) + WORLD_DIMENSION / 2.0f, center.getY( ) - 10.0f, center.getZ( ) - WORLD_DIMENSION / 8.0f );

		glBindTexture( GL_TEXTURE_2D, 0 );
		
			for( size_t i = 0; i < nNbrOfParticles; i++ )
			{
				
				if( particleArray[ i ].m_ptPosition.getDistanceFrom( theCamera.xPos, theCamera.yPos, theCamera.zPos ) <= 100.0f )
				{
					glBegin( GL_TRIANGLE_STRIP );
					glColor4f( 0.2f, 0.2f, 0.3f, 1.0f);
					x = particleArray[ i ].m_ptPosition.getX( );
					y = particleArray[ i ].m_ptPosition.getY( );
					z = particleArray[ i ].m_ptPosition.getZ( );

					//if( frustum.isPointInFrustum( x, y, z ) == false ) continue;

					fSize = particleArray[ i ].m_Size;
	
			
					glTexCoord2d( 1, 1 ); glVertex3f( x + fSize, y + fSize, z );		// Top Right
					glTexCoord2d( 0, 1 ); glVertex3f( x - fSize, y + fSize, z );		// Top Left
					glTexCoord2d( 1, 0 ); glVertex3f( x + fSize, y - 5 * fSize, z );	// Bottom Right
					glTexCoord2d( 0, 0 ); glVertex3f( x - fSize, y - 5 * fSize, z );	// Bottom Left
					glEnd( );
				}
				
			}
		
	glPopMatrix( );
	
	glDisable( GL_ALPHA_TEST );
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE_2D );
	glEnable( GL_CULL_FACE );
}

void CRain::Step( )
{
	if( m_RainParticles.getParticlesCount( ) == 0 )
		m_RainParticles.Emits( m_NumberOfRainParticles/*NUMBER_OF_RAINPARTICLES*/,  C3DPoint<float>( 0, 100, 0 ) );  // for testing
		//m_RainParticles.Emits( NUMBER_OF_RAINPARTICLES, C3DLine<float>( pt1, pt2 ) );

	//m_RainParticles.Update( );
	m_RainParticles.UpdateAndRespawn( );
}