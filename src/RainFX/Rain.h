#pragma once
#ifndef __RAIN_H__
#define __RAIN_H__
/*
 *	Rain.h
 *
 *
 */
#include "../Renderable.h"
#include "../Math/3DPoint.h"
#include "../ParticleSystem/ParticlePolicies/InitializePolicy.h"
#include "../ParticleSystem/ParticlePolicies/ActionPolicy.h"
#include "../ParticleSystem/ParticlePolicies/CompletePolicy.h"
#include "../ParticleSystem/ParticlePolicies/NullPolicy.h"
#include "../ParticleSystem/Particle.h"
#include "../ParticleSystem/ParticleGroup.h"
#include "../TimerMain.h"
//#include "../Timer/Timer.h"


#define WORLD_DIMENSION		100
#define RAIN_SIZE			0.3f
#define NUMBER_OF_RAINPARTICLES		1024	


class CRain : public CRenderable
{
	friend class RainParticleInitializer;
	friend class RainWindPolicy;
	friend class RainParticleLifePolicy;


  public:
	CRain( void );
	CRain(const float x, const float y, const float z, Vector3<float> windVelocity, double amountOfRain );
	~CRain( void );

	void Draw( );
	void Step( );
  private:
	void initialize( );
	
	unsigned int m_RainDrop; 
	static Vector3<float> m_WindVelocity;
	double m_AmountOfRain; 
	C3DPoint<float> pt1, pt2;
	
	class RainParticleInitializer : public InitializePolicy<Particle>
	{
		friend class CRain;
	  public:
		inline virtual void operator()( Particle &part )
		{
 			//initialize position; Y component is already set.
			float x = WORLD_DIMENSION * ((float) rand( ) / (float) RAND_MAX);
			part.m_ptPosition.setX( -WORLD_DIMENSION + x );
			//float y = 20.0f * ((float) rand( ) / RAND_MAX) + 50.0f;
			float y = 60.0f * ((float) rand( ) / (float) RAND_MAX) + 10.0f;
			part.m_ptPosition.setY( y );
			float z = 2*WORLD_DIMENSION * ((float) rand( ) / (float) RAND_MAX);
			part.m_ptPosition.setZ( -WORLD_DIMENSION + z );

			


			part.m_Size = RAIN_SIZE;

			// move this much after its working
			part.m_vVelocity = CRain::m_WindVelocity;
			//part.m_vVelocity.setX( 0.02f );
			//part.m_vVelocity.setY( -0.01f );
			//part.m_vVelocity.setZ( 0.015f );
			part.m_nLifeSpan = 1;
		}
		inline virtual void PrepareAction( )
		{}
	};
	

	template <class wind = double>
	class RainWindPolicy : public ActionPolicy<Particle> {
		
	  private:
		Vector3<float> m_vWind;
	  public:
		inline virtual void operator()( Particle &part )
		{
			//part.m_vVelocity = m_vWind;
		}
		inline virtual void PrepareAction( )
		{}

		RainWindPolicy( ) : m_vWind( 0.05f, 0.0f, 2.0f)
		{}
		
	};
	
	class RainParticleLifePolicy : public ActionPolicy<Particle> {
	  public:
		inline virtual void operator()( Particle &part )
		{
			// Kill a rain drop if it falls through the floor
			if( part.m_ptPosition.getY( ) <= 6*RAIN_SIZE )
				part.m_nLifeSpan = 0;
			if( part.m_ptPosition.getX(  ) > WORLD_DIMENSION || part.m_ptPosition.getX(  ) < -WORLD_DIMENSION )
				part.m_nLifeSpan = 0;
			if( part.m_ptPosition.getZ(  ) > WORLD_DIMENSION || part.m_ptPosition.getZ(  ) < -WORLD_DIMENSION )
				part.m_nLifeSpan = 0;
		}
		inline virtual void PrepareAction( )
		{}

		RainParticleLifePolicy( ) 
		{}

        	
	};
	
	class RainParticleMovementPolicy : public ActionPolicy<Particle> {
	  public:
		inline virtual void operator()( Particle &part )
		{
			const float x = part.m_ptPosition.getX( ),
						y = part.m_ptPosition.getY( ),
						z = part.m_ptPosition.getZ( );

			

			part.m_ptPosition.setX( part.m_vVelocity.getX( ) * TimeStep + x );
			part.m_ptPosition.setY( part.m_vVelocity.getY( ) * TimeStep + y );
			part.m_ptPosition.setZ( part.m_vVelocity.getZ( ) * TimeStep + z );
		}
		inline virtual void PrepareAction( ){}	
	};

 


	ParticleGroup<NUMBER_OF_RAINPARTICLES, RainParticleInitializer, 
		CompletePolicy<Particle, NullPolicy<Particle>, NullPolicy<Particle>, NullPolicy<Particle>, RainParticleLifePolicy, RainParticleMovementPolicy>,
		Particle >  m_RainParticles;


	C3DPoint<float> center;
	float xTheta, yTheta, zTheta;
	unsigned int m_NumberOfRainParticles;
	static const unsigned int MAX_RAIN;
};

#endif