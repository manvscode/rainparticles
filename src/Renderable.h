#pragma once
#ifndef __RENDERABLE_H__
#define __RENDERABLE_H__

class CRenderable
{
public:

	CRenderable(void)
	{
	}

	virtual ~CRenderable(void)
	{
	}

	virtual void Draw( ) = 0;
};

#endif