/*
 *		WaterRipple.cpp
 *
 *		Draws water ripples using height fields.
 *		Coded by Joseph A. Marrero
 *		10/17/05
 */
//#include "../main.h"
#include "../opengl.h"
#include "../TimerMain.h"
#include "WaterRipple.h"
#include <cassert>
#include <vector>

template <unsigned int Width, unsigned int Height>
CWaterRipple<Width,Height>::CWaterRipple( const float x, const float y, const float z, const float spacing, const float wave_speed, const float viscosity )
:  m_D(spacing), m_V(wave_speed), m_Viscosity(viscosity), m_TimeCondition(0.0f)
{
	m_X = x; m_Y = y; m_Z = z;
	//m_NumberOfXParts = Width / m_D;
	//m_NumberOfZParts = Height / m_D;

	//assert( m_V < 0 || m_V > (m_D /(2 * TimeStep)) * sqrt( m_Viscosity * TimeStep + 2 ) );
	//assert( m_V > 0 && m_V < (m_D/(2*TimeStep)) * sqrt( m_Vocosity*TimeStep + 2 ) );

	clearWater( );

	
	m_TimeCondition = (float) ( viscosity + sqrt(viscosity * viscosity + (32.0f * wave_speed * wave_speed / spacing * spacing) ) ) / (8.0f * wave_speed * wave_speed / spacing * spacing  );
	//srand( (unsigned int) timer.GetTime( ) );
}

template <unsigned int Width, unsigned int Height>
CWaterRipple<Width,Height>::~CWaterRipple(void)
{
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::setRandomRipple( )
{
	setRipple( ((float) rand( ) / RAND_MAX) * Width, ((float) rand( ) / RAND_MAX) * Height );
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::setRipple( const unsigned int x, const unsigned int y )
{
	assert( x >= 0 && y >= 0 && x <= Width - 1 && y <= Height -1 );
	currentBuffer[ x ][ y ] += 0.5f * ( rand( ) % 100 ) / 100.0f;
	//currentBuffer[ x ][ y ] = 2.0f;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::Draw( )
{	
	//
	//Vertex3 vert; 

	//for(float x = 0; x < Width; x+=m_D)
	//{
	//	float z = 0;

	//	vert.x = x;
	//	vert.y = nextBuffer[(int)x][(int)z];
	//	vert.z = z;
	//	m_vertArray.push_back(vert);

	//	for(; z < Height; z+=m_D)
	//	{
	//		vert.x = x;
	//		vert.y = nextBuffer[(int)x][(int)z];
	//		vert.z = z;
	//		m_vertArray.push_back(vert);


	//		vert.x = (x + m_D);
	//		vert.y = (nextBuffer[(int)x+1][(int)z]);
	//		vert.z = z;
	//		m_vertArray.push_back(vert);
	//	}

	//	m_vertArray.push_back(vert);
	//}	
	//
	//glPushMatrix( );
	//	glTranslatef( m_X - (Width * m_D / 2.0f), m_Y, m_Z - (Height * m_D / 2.0f) );	
	//	glColor3f( 0.2f, 0.1, 0.8f);
	//	glNormal3f(0.0f, 1.0f, 0.0f);
	//
	//	glVertexPointer( 3, GL_FLOAT, 0, &( m_vertArray[ 0 ] ) );
	//	glDrawArrays( GL_LINE_STRIP, 0, m_vertArray.size( ) );
	//glPopMatrix();

	//m_vertArray.clear( );



	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glPushMatrix( );
		glTranslatef( m_X - (Width * m_D / 2.0f), m_Y, m_Z - (Height * m_D / 2.0f) );
		glLineWidth( 0.0f );
		glBegin( GL_TRIANGLE_STRIP );
			
			glColor3f( 0.0f, 0.2f, 0.95f );
			for( register unsigned int x = 0; x < Width; x++ )
			{
				register unsigned int z = 0;
				glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );

				for( ; z < Height; z++ )
				{
					glColor3f( 0.0f, 0.2f, 0.95f );
					Vector3<float> v1(1.0f, nextBuffer[ x + 1 ][ z + 1 ] - nextBuffer[ x ][ z ], 1.0f );
					Vector3<float> v2(1.0f, nextBuffer[ x + 1 ][ z ] - nextBuffer[ x ][ z ], 0.0f );
					Vector3<float> r = v1.CrossProduct( v2 );
					r.Normalize( );
					glNormal3f( r.getX( ), r.getY( ), r.getZ( ) );
					glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );
					glVertex3f( ( x + 1 ) * m_D, nextBuffer[ x + 1 ][ z ], z * m_D );
				}
				glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );
			}
		glEnd( );
	glPopMatrix( );
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::Update( )
{
	float t = TimeStep;
	assert( t > 0.0f );


	// Test Constraints
	if( !isTimeConditionMet() )
	{
		//clearWater( );
		t = 0.5f * m_TimeCondition;
	}
	WaveSpeedConditionTestAndHandle( );
	
	//float d = t;
	float d = (m_Viscosity * t + 2.0f);
	//assert( d != 0.0f && abs(d) > 0.005f );

	firstCoefficient = (4.0f - (8.0f * m_V * m_V * t * t / (m_D * m_D))) / d;
	secondCoefficient = (m_Viscosity * t - 2.0f) / d;
	thirdCoefficient = (2.0f * m_V * m_V * t * t / (m_D * m_D)) / d;

	//assert( abs(nextBuffer[ 0 ][ 0 ]) < 100.0f );

	for( register unsigned int x = 1; x < Width-1; x++ )
		for( register unsigned int z = 1; z < Height-1; z++ )
		{
			nextBuffer[ x ][ z ] = firstCoefficient * currentBuffer[ x ][ z ] + secondCoefficient * previousBuffer[ x ][ z ]
									+ thirdCoefficient * ( currentBuffer[ x + 1 ][ z ] + currentBuffer[ x - 1 ][ z ] + currentBuffer[ x ][ z + 1 ] + currentBuffer[ x ][ z - 1 ] );
		
			//if( nextBuffer[ x ][ z ] > 25.0 ) nextBuffer[ x ][ z ] = 25.0; // This line is a hack; remove it later.
		}
	
	memcpy( previousBuffer, currentBuffer, sizeof( float ) * Width * Height ); 
	memcpy( currentBuffer, nextBuffer, sizeof( float ) * Width * Height );

	
}


template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::ClearBackBuffer( )
{
	//for( register float x = 0.0f; x < m_Width; x++ )
	//	for( register float z = 0.0f; z < m_Height; z++ )
	//		m_Buffer1[ x ][ z ] = 0.0f;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::ClearRipples( )
{
	for( register unsigned int x = 0.0f; x < Width; x++ )
		for( register unsigned int z = 0.0f; z < Height; z++ )
			currentBuffer[ x ][ z ] = 0.0f;
}

template <unsigned int Width, unsigned int Height>
inline bool CWaterRipple<Width,Height>::isTimeConditionMet( )
{
	return (abs( TimeStep ) < m_TimeCondition);
}

template <unsigned int Width, unsigned int Height>
inline bool CWaterRipple<Width,Height>::isWaveSpeedConditionMet( )
{
	const float waveSpeedCondition = (m_D/(2.0f*TimeStep)) * sqrt(m_Viscosity*TimeStep + 2.0f);

	if( abs( m_V ) < waveSpeedCondition )
	{
		//adjust wave speed
		m_V = 0.5f * waveSpeedCondition;
		return false;
	}
	return true;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::WaveSpeedConditionTestAndHandle( )
{
	const float waveSpeedCondition = (m_D/(2.0f*TimeStep)) * sqrt(m_Viscosity*TimeStep + 2.0f);

	if( abs( m_V ) < waveSpeedCondition )
	{
		//adjust wave speed
		//assert(false);
		m_V = 0.5f * waveSpeedCondition;
		return;
	}
}

template <unsigned int Width, unsigned int Height>

void CWaterRipple<Width,Height>::clearWater( )
{
	for( register unsigned int x = 0; x < Width; x++ )
		for( register unsigned int z = 0; z < Height; z++ )
			nextBuffer[ x ][ z ] = currentBuffer[ x ][ z ] = previousBuffer[ x ][ z ] = 0.0f;
}