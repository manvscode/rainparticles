#pragma once
#ifndef __WATERRIPPLE_H__
#define __WATERRIPPLE_H__
/*
 *		WaterRipple.h
 *
 *		Draws water ripples using height fields.
 *		Coded by Joseph A. Marrero
 *		10/17/05
 */
#include "../Renderable.h"
//#include "../Math/3DPoint.h"
//#include "../Math/Vector3.h"
#include <vector>



template <unsigned int Width = 25, unsigned int Height = 25>
class CWaterRipple : public CRenderable
{
  public:
	explicit CWaterRipple( const float x,
						   const float y,
						   const float z,
						   const float spacing,
						   const float wave_speed,
						   const float viscosity );
	virtual ~CWaterRipple( );

	void setRandomRipple( );
	void setRipple( const unsigned int x, const unsigned int y );
	void setRipple( const unsigned int x, const unsigned int y, const float height );
	void ClearRipples( );
	void Update( );
	void Draw( );
	void Step( )
	{
		Update( ); 
	}
	
	void clearWater( );

  private:

	  typedef struct tagVertex {
			float x, y, z;
	  } Vertex;

	  float m_X, m_Y, m_Z;
	  //float m_NumberOfXParts, m_NumberOfZParts;
	  float m_V, m_D, m_Viscosity;
	  float firstCoefficient,
		    secondCoefficient,
			thirdCoefficient;

	  void initialize( );
	  std::vector<Vertex> nextBuffer,
			currentBuffer,
			previousBuffer;
	  
	  float m_TimeCondition;
	  inline bool isTimeConditionMet( );
	  inline bool isWaveSpeedConditionMet( );
	  void WaveSpeedConditionTestAndHandle( );
	
};
#include "WaterRipple.inl"
#endif