/*
 *		WaterRipple.cpp
 *
 *		Draws water ripples using height fields.
 *		Coded by Joseph A. Marrero
 *		10/17/05
 */
//#include "../main.h"
#include "../opengl.h"
#include "../TimerMain.h"
#include "WaterRipple.h"
#include <cassert>
#include <vector>

template <unsigned int Width, unsigned int Height>
CWaterRipple<Width,Height>::CWaterRipple( const float x, const float y, const float z, const float spacing, const float wave_speed, const float viscosity )
:  m_D(spacing), m_V(wave_speed), m_Viscosity(viscosity), m_TimeCondition(0.0f)
{
	m_X = x; m_Y = y; m_Z = z;

	m_TimeCondition = (float) ( viscosity + sqrt(viscosity * viscosity + (32.0f * wave_speed * wave_speed / spacing * spacing) ) ) / (8.0f * wave_speed * wave_speed / spacing * spacing  );

	float t = 0.22f * m_TimeCondition;//TimeStep;
	float d = (m_Viscosity * t + 2.0f);
	firstCoefficient = (4.0f - (8.0f * m_V * m_V * t * t / (m_D * m_D))) / d;
	secondCoefficient = (m_Viscosity * t - 2.0f) / d;
	thirdCoefficient = (2.0f * m_V * m_V * t * t / (m_D * m_D)) / d;
	
	//srand( (unsigned int) timer.GetTime( ) );
	glEnableClientState( GL_VERTEX_ARRAY );
	initialize( );
	clearWater( );
}

template <unsigned int Width, unsigned int Height>
CWaterRipple<Width,Height>::~CWaterRipple( )
{
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::initialize( )
{
	Vertex vert;

	// create the tri-strip...
	for(float x = 0; x < Width; x += m_D)
	{
		float z = 0;

		vert.x = x;
		vert.y = 0;
		vert.z = z;
		nextBuffer.push_back(vert);
		currentBuffer.push_back(vert);
		previousBuffer.push_back(vert);

		for( ; z < Height; z += m_D)
		{
			vert.x = x;
			vert.y = 0;
			vert.z = z;
			nextBuffer.push_back(vert);
			currentBuffer.push_back(vert);
			previousBuffer.push_back(vert);

			vert.x = (x + m_D);
			vert.y = 0;
			vert.z = z;
			nextBuffer.push_back(vert);
			currentBuffer.push_back(vert);
			previousBuffer.push_back(vert);
		}

		nextBuffer.push_back(vert);
		currentBuffer.push_back(vert);
		previousBuffer.push_back(vert);
	}	
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::setRandomRipple( )
{
	setRipple( ((float) rand( ) / RAND_MAX) * Width, ((float) rand( ) / RAND_MAX) * Height );
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::setRipple( const unsigned int x, const unsigned int y )
{
	unsigned int index_for_xy = x * Height + y;
	assert( x >= 0 && y >= 0 && x <= Width - 1 && y <= Height -1 );
	currentBuffer[ index_for_xy ].y += 0.5f * ( rand( ) % 100 ) / 100.0f;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::setRipple( const unsigned int x, const unsigned int y, const float height )
{
	unsigned int index_for_xy = x * Height + y;
	assert( x >= 0 && y >= 0 && x <= Width - 1 && y <= Height -1 );
	currentBuffer[ index_for_xy ].y += height ;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::Draw( )
{	

	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glPushMatrix( );
		glTranslatef( m_X - (Width * m_D / 2.0f), m_Y, m_Z - (Height * m_D / 2.0f) );
		glLineWidth( 0.5f );
		//glBegin( GL_TRIANGLE_STRIP );

		//	glColor4f( 0.0f, 0.2f, 0.95f, 0.2f );
		//	for( register unsigned int x = 0; x < Width; x++ )
		//	{
		//		register unsigned int z = 0;
		//		glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );

		//		for( ; z < Height; z++ )
		//		{
		//			
		//			Vector3<float> v1(1.0f, nextBuffer[ x + 1 ][ z + 1 ] - nextBuffer[ x ][ z ], 1.0f );
		//			Vector3<float> v2(1.0f, nextBuffer[ x + 1 ][ z ] - nextBuffer[ x ][ z ], 0.0f );
		//			Vector3<float> r = v1.crossProduct( v2 );
		//			r.normalize( );
		//			glNormal3f( r.getX( ), r.getY( ), r.getZ( ) );
		//			glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );
		//			glVertex3f( ( x + 1 ) * m_D, nextBuffer[ x + 1 ][ z ], z * m_D );
		//		}
		//		glVertex3f( x * m_D, nextBuffer[ x ][ z ], z * m_D );
		//	}
		//glEnd( );



		glColor4f( 0.0f, 0.2f, 0.95f, 0.2f );
		glNormal3f(0.0f, 1.0f, 0.0f);
		
		glVertexPointer( 3, GL_FLOAT, 0, &nextBuffer[ 0 ] );
		glDrawArrays( GL_TRIANGLE_STRIP, 0, nextBuffer.size( ) );

	glPopMatrix( );
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::Update( )
{
	unsigned int index_for_xz = 0;
	unsigned int index_for_x_plus_one_z = 0;
	unsigned int index_for_x_minus_one_z = 0;
	unsigned int index_for_x_z_plus_one = 0;
	unsigned int index_for_x_z_minus_one = 0;

	for( register unsigned int x = 1; x < Width-1; x++ )
		for( register unsigned int z = 1; z < Height-1; z++ )
		{
			//nextBuffer[ x ][ z ] = firstCoefficient * currentBuffer[ x ][ z ] + secondCoefficient * previousBuffer[ x ][ z ]
			//						+ thirdCoefficient * ( currentBuffer[ x + 1 ][ z ] + currentBuffer[ x - 1 ][ z ] + currentBuffer[ x ][ z + 1 ] + currentBuffer[ x ][ z - 1 ] );
			index_for_xz = x * Width + z;
			index_for_x_plus_one_z = (x + 1) * Width + z;
			index_for_x_minus_one_z = (x - 1) * Width + z;
			index_for_x_z_plus_one = x * Width + (z + 1);
			index_for_x_z_minus_one = x * Width + (z - 1);

			nextBuffer[ index_for_xz ].y = firstCoefficient * currentBuffer[ index_for_xz ].y + secondCoefficient * previousBuffer[ index_for_xz ].y
								  + thirdCoefficient * ( currentBuffer[ index_for_x_plus_one_z ].y + currentBuffer[ index_for_x_minus_one_z ].y + currentBuffer[ index_for_x_z_plus_one ].y + currentBuffer[ index_for_x_z_minus_one ].y );
		}
	
	//memcpy( previousBuffer, currentBuffer, sizeof( float ) * Width * Height ); 
	//memcpy( currentBuffer, nextBuffer, sizeof( float ) * Width * Height );

	previousBuffer = currentBuffer;
	currentBuffer = nextBuffer;
	//memcpy( &previousBuffer[ 0 ], &currentBuffer[ 0 ], sizeof( Vertex ) * currentBuffer.size( ) ); 
	//memcpy( &currentBuffer[ 0 ], &nextBuffer[ 0 ], sizeof( Vertex ) * nextBuffer.size( ) );
	
}


template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::ClearRipples( )
{
	for( register unsigned int x = 0.0f; x < Width; x++ )
		for( register unsigned int z = 0.0f; z < Height; z++ )
			currentBuffer[ x ][ z ] = 0.0f;
}

template <unsigned int Width, unsigned int Height>
inline bool CWaterRipple<Width,Height>::isTimeConditionMet( )
{
	return (abs( TimeStep ) < m_TimeCondition);
}

template <unsigned int Width, unsigned int Height>
inline bool CWaterRipple<Width,Height>::isWaveSpeedConditionMet( )
{
	const float waveSpeedCondition = (m_D/(2.0f*TimeStep)) * sqrt(m_Viscosity*TimeStep + 2.0f);

	if( abs( m_V ) < waveSpeedCondition )
	{
		//adjust wave speed
		m_V = 0.5f * waveSpeedCondition;
		return false;
	}
	return true;
}

template <unsigned int Width, unsigned int Height>
void CWaterRipple<Width,Height>::WaveSpeedConditionTestAndHandle( )
{
	const float waveSpeedCondition = (m_D/(2.0f*TimeStep)) * sqrt(m_Viscosity*TimeStep + 2.0f);

	if( abs( m_V ) < waveSpeedCondition )
	{
		//adjust wave speed
		//assert(false);
		m_V = 0.5f * waveSpeedCondition;
		return;
	}
}

template <unsigned int Width, unsigned int Height>

void CWaterRipple<Width,Height>::clearWater( )
{
	for( register unsigned int x = 0; x < Width; x++ )
		for( register unsigned int z = 0; z < Height; z++ )
			nextBuffer[ x * Height + z ].y = currentBuffer[ x * Height + z ].y = previousBuffer[ x * Height + z ].y = 0.0f;
}