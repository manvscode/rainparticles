/*
 *		TextureManager.cpp
 *
 *		A manager of textures.
 *		Coded by Joseph A. Marrero
 *		10/11/05
 */

#include "TextureManager.h"

CTextureManager::CTextureManager(void)
{
}

CTextureManager::~CTextureManager(void)
{
}

CTextureManager *CTextureManager::m_pInstance = 0;

const CTextureManager *CTextureManager::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new CTextureManager( );
	
	return m_pInstance;
}