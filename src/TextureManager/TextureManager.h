#pragma once
#ifndef __TEXTUREMANAGER_H__
#define __TEXTUREMANAGER_H__
/*
 *		TextureManager.h
 *
 *		A manager of textures.
 *		Coded by Joseph A. Marrero
 *		10/11/05
 */

class CTextureManager
{
  public:
	virtual ~CTextureManager(void);
	const CTextureManager *getInstance( );
  private:
	CTextureManager(void);
	static CTextureManager *m_pInstance;
};


#endif