/*		Timer.cpp
 *
 *		Timer object to keep time from a initial moment.
 *		Coded by Joseph A. Marrero
 */
//
//	Note: You must use Winmm.lib
//

#include <windows.h>
#include <mmsystem.h>
#include "timer.h"

Timer::Timer(void)
{
	InitTimer();
}

Timer::~Timer(void)
{
	
}
void Timer::InitTimer()
{
	if( !QueryPerformanceFrequency( (LARGE_INTEGER *)&ticksPerSecond))
		ticksPerSecond = 1000;

	timeFromStart = 0;
	timeFromStart = GetTime();
}
void Timer::ResetTimer()
{
	timeFromStart = 0;
	timeFromStart = GetTime();
}
bool Timer::Wait(float time_secs)
{
	float t_1 = GetTime();

	while(  GetTime() - t_1 <= time_secs);
	return true;
}
float Timer::GetTime()
{
	UINT64	ticks;
	float	time;

	//Get time since start
	if( !QueryPerformanceCounter( (LARGE_INTEGER *) &ticks) )
		ticks = (UINT64) timeGetTime();
	
	//Divide the freq. to get the time in seconds
	time = (float)(__int64)ticks/(float)(__int64)ticksPerSecond;
	
	//Subtract the time from start
	time -= timeFromStart;

	return time;
}