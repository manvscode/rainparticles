/*
 *		main.cpp
 *
 *		Coded by Joseph A. Marrero
 */


#pragma once
#define WIN32_LEAN_AND_MEAN
#include <cassert>
#include "Main.h"
#include "Camera.h"
#include "Resource.h"
//#include "TimerMain.h"
#include "RainFX/Rain.h"
#include "RippleFX/WaterRipple.h"



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	WNDCLASSEX	wndClass;
	HWND hWnd;
	MSG msg;
	bool done = false;
	RECT windowRect;
	DWORD windowStyle, extendedWindowStyle;


	windowRect.top = 0;
	windowRect.left = 0;
	windowRect.bottom = RESOLUTION_Y;
	windowRect.right = RESOLUTION_X;


	wndClass.cbSize = sizeof( WNDCLASSEX );
	wndClass.style = CS_HREDRAW | CS_VREDRAW;
	wndClass.lpfnWndProc = WndProc;
	wndClass.cbClsExtra = 0;
	wndClass.cbWndExtra = 0;
	wndClass.hInstance = hInstance;
	wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = "RainSim";
	wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON));

	if( !RegisterClassEx(&wndClass) ){
		MessageBox(NULL,"Unable to register window class!", "Error", MB_OK |MB_ICONERROR);
		return -1;
	}

	hWnd = CreateWindowEx(NULL, "RainSim", "Rain Simulation",WS_POPUP | WS_EX_TOPMOST, 0, 0, RESOLUTION_X, RESOLUTION_Y, NULL, NULL, hInstance, NULL);
	
	if(!hWnd){
		MessageBox(NULL,"Unable to create window!", "Error", MB_OK |MB_ICONERROR);
		return -1;
	}
	ShowWindow(hWnd, SW_SHOW);
	
	if(fullscreen){
		//GoFullscreen(RESOLUTION_X, RESOLUTION_Y, BIT_RATE);

		extendedWindowStyle = WS_EX_APPWINDOW;
		windowStyle = WS_POPUP;
		ShowCursor(FALSE);
	}
	else {
		extendedWindowStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
		windowStyle = WS_OVERLAPPEDWINDOW; //|	WS_VISIBLE | WS_SYSMENU | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	}
	
	AdjustWindowRectEx(&windowRect, windowStyle, FALSE, extendedWindowStyle);
	UpdateWindow(hWnd);

	theCamera.xPos = 0.0f;
	theCamera.yPos = -20.0f;
	theCamera.zPos = -85.0f;

	timer.ResetTimer( );
	CRain rain( 0.0f, 0.0f, 0.0f, Vector3<float>( -0.01f, -175.0f, -2.0f ), 0.5f );

	CWaterRipple<50, 50> waterRipple( 0.0f, 0.0f, 0.0f, 1.0f, 5.0f, 1.0f );
	//CWaterRipple<250, 250> waterRipple( 0.0f, 0.0f, 0.0f, 0.4f, 5.0f, 1.0f );

	
	float to = 0.0f;
	size_t frameCount = 0;

	while( !done )
	{
	
		if( PeekMessage( &msg, NULL, 0, 0, PM_REMOVE ) )
		{
			if( msg.message == WM_QUIT )
				break;
			
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else {
			//to = timer.GetTime( );
			TimeStep = (timer.GetTime( ) - to);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
			glClearColor( 0.2f, 0.2f, 0.2f, 0.0f );
			glLoadIdentity();

			// Position the Camera
			
			// User Camera Movement
			glRotatef( (GLfloat) theCamera.xAngle, 1.0f, 0.0f, 0.0f );
			glRotatef( (GLfloat) theCamera.yAngle, 0.0f, 1.0f, 0.0f );
			glTranslatef( (GLfloat) theCamera.xPos, (GLfloat) theCamera.yPos, (GLfloat) theCamera.zPos );
			
			// Rotate the camera
			static float yTheta = 0.0f,
						 Theta = 0.0f,
						 rotationalDecay;
	
			rotationalDecay = 12.0f * sin(3*Theta) + 13.0f;//* (Theta / 180.0f);
			
			glRotatef( rotationalDecay, 1.0f, 0.0f, 0.0f );
			glRotatef( rotationalDecay, 1.0f, 0.0f, 1.0f );
			Theta += 0.003f;
			if( Theta >= 360.0 ) Theta = 0.0f;

			glRotatef( yTheta += 0.25f, 0.0f, 1.0f, 0.0f );
			if( yTheta >= 360.0f ) yTheta = 0.0f;

			
			//waterRipple.setRipple( 20*sin(Theta) + 125, 20*sin(Theta) + 125, 0.25f);

			//frustum.calculateFrustum( );

#ifdef WORLDBOX // Bounding Box
			glDisable( GL_LIGHTING );
			glBegin( GL_LINE_STRIP );
				glLineWidth( 4.0f );
				glColor3f( 1.0f, 0, 0 );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, 0 , -WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, 0 , -WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, 0 , WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, 0 , WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, 0 , -WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, WORLD_DIMENSION, -WORLD_DIMENSION / 2.0f );
				
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, WORLD_DIMENSION , -WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, WORLD_DIMENSION, WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, WORLD_DIMENSION, WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, WORLD_DIMENSION, -WORLD_DIMENSION / 2.0f );
			glEnd( );
			glBegin( GL_LINES );
				glLineWidth( 4.0f );
				glColor3f( 1.0f, 0, 0 );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, WORLD_DIMENSION , -WORLD_DIMENSION / 2.0f );
				glVertex3f( WORLD_DIMENSION / 2.0f, 0 , -WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, WORLD_DIMENSION , WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( WORLD_DIMENSION / 2.0f, 0 , WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, WORLD_DIMENSION , WORLD_DIMENSION / 2.0f );
				glFogCoordfEXT( 1.0f ); 
				glVertex3f( -WORLD_DIMENSION / 2.0f, 0 , WORLD_DIMENSION / 2.0f );
			glEnd( );
			glEnable( GL_LIGHTING );
#endif
			waterRipple.Draw( );
			rain.Draw( );
	
			waterRipple.Step( );
			rain.Step( );
			

			
			



			

			SwapBuffers( hDC );
			frameCount++;

			//TimeStep = (timer.GetTime( ) - to);
			to = timer.GetTime( );

			if( TimeStep < SIXTY_FPS )
				Sleep( (DWORD) SIXTY_FPS - TimeStep );
			if( frameCount >= 200 )
			{
				srand( (unsigned int) timer.GetTime( ) );
				frameCount = 0;
			}
			else if( frameCount > 25 )
			{
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
			}
			else if( frameCount > 63 )
			{
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
			}
			else if( frameCount > 100 )
			{
				waterRipple.setRandomRipple( );
				waterRipple.setRandomRipple( );
			}



		}
	}

	ImageIO::destroyTGAImageData( &RainTextureTGAFile );
	return 0;
}
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT		ps;
	static HGLRC	hRC;
	int				width,
					height;

	switch( message )
	{
		case WM_CREATE:
		
			hDC = GetDC( hwnd );
			SetupPixelDescriptor( hDC );
			hRC = wglCreateContext( hDC );
			wglMakeCurrent( hDC, hRC );
			
			ImageIO::loadTGAFile( RAINTEXTURE_FILE, &RainTextureTGAFile );
			
			extensionInit( );
			GLInitialize( );
			
			glLoadIdentity( );
			//return 0;
			break;
		case WM_PAINT:
			BeginPaint( hwnd, &ps );
			EndPaint( hwnd, &ps );
			return 0;
			break;
		case WM_CLOSE:
			ReleaseDC( hwnd, hDC );

			wglMakeCurrent( hDC, NULL );
			wglDeleteContext( hRC );
			PostQuitMessage( 0 );
			return 0;
			break;
		case WM_DESTROY:
			ReleaseDC( hwnd, hDC );
			break;
		case WM_SIZE:
			height = HIWORD( lParam );
			width = LOWORD( lParam );
			
			if( height == 0 )
				height = 1;

			glViewport( 0, 0, width, height );
			
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity( );

			gluPerspective( 45.0f, (GLfloat) width / (GLfloat) height, 0.1f, 1000.0f );
			//glOrtho(0.0f, width - 1.0, 0.0, height - 1.0, -1.0, 1.0);
			
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity( );
			//return 0;
			break;
		case WM_KEYUP:						
		{
			keys[ wParam ] = false;				
			return 0;
			break;
		}
		case WM_KEYDOWN:					
		{
			keys[ wParam ] = true;	

			if( keys[ VK_ESCAPE ] == true )
			{	
				ReleaseDC( hwnd, hDC );
				wglMakeCurrent( hDC, NULL );
				wglDeleteContext( hRC );
				PostQuitMessage( 0 );
				return 0;
			}
			else {
				switch( wParam )
				{
					case 87: // 'W'
						if( theCamera.xAngle >= 360.0f )
							theCamera.xAngle = 0.0f;
						else
							theCamera.xAngle += 1.0f;
						break;
		
					case 83: // 'S'
						if( theCamera.xAngle <= 0.0f )
							theCamera.xAngle = 0.0f;
						else
							theCamera.xAngle += -1.0f;
						break;
					case 65: // 'A'
						if( theCamera.yAngle <= -360.0f )
							theCamera.yAngle = 0.0f;
						else
							theCamera.yAngle += -1.0f;
						break;
					case 68: // 'D'
						if( theCamera.yAngle >= 360.0f )
							theCamera.yAngle = 0.0f;
						else
							theCamera.yAngle += 1.0f;
						break;
					case VK_INSERT:
						theCamera.zPos += 1.5f;
						break;
					case VK_DELETE:
						theCamera.zPos += -1.5f;
						break;
					case VK_LEFT:
						theCamera.xPos += 1.0f;
						break;
					case VK_RIGHT:
						theCamera.xPos += -1.0f;
						break;
					case VK_UP:
						if( theCamera.yPos < -45.0f )
							theCamera.yPos = -45.0f;
						else				
							theCamera.yPos += -1.0f;
						break;
					case VK_DOWN:
						if(  theCamera.yPos > -45.0f && theCamera.yPos > 0.0f )
							theCamera.yPos = 0.0f;
						else
							theCamera.yPos += 1.0f;

						break;
					default:
 						break;
				}
			}
				
			//return 0;
			break;
		}

		case WM_SYSCOMMAND:						
			{
				switch( wParam )						
				{
					case SC_SCREENSAVE:				
					case SC_MONITORPOWER:
						break;
					//return 0;					
				}
			break;							
			}

		default:
			break;
	}
	return DefWindowProc(hwnd,message, wParam,lParam);
}

void GLInitialize( )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );
	

	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	//glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	//glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );							// Depth Buffer Setup
	glEnable( GL_DEPTH_TEST );
	glDepthFunc( GL_ALWAYS );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );
	glFrontFace( GL_CCW );
	
	//glDepthMask(GL_TRUE);
	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );

	for( register int i = 0; i < 256; i++ )
		keys[ i ] = false;

	// texturing
	//glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, RainTextureName );

	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	assert( !glGetError( ) );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, RainTextureTGAFile.imageWidth, RainTextureTGAFile.imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, RainTextureTGAFile.imageData );

	// Set Up Fog 
	glEnable( GL_FOG );							// Enable Fog
	glFogi( GL_FOG_MODE, GL_LINEAR );						// Fog Fade Is Linear
	glFogfv( GL_FOG_COLOR, fogColor );					// Set The Fog Color
	glFogf( GL_FOG_START,  0.0f );						// Set The Fog Start (Least Dense)
	glFogf( GL_FOG_END,    2.0f );						// Set The Fog End (Most Dense)
	glHint( GL_FOG_HINT, GL_FASTEST );						// Per-Pixel Fog Calculation
	glFogi( GL_FOG_COORDINATE_SOURCE_EXT, GL_FOG_COORDINATE_EXT );		// Set Fog Based On Vertice Coordinates



	glEnable( GL_LIGHTING );
	glLightfv( GL_LIGHT0, GL_SPECULAR, WorldLight );
	glLightfv( GL_LIGHT0, GL_POSITION, WorldLightPosition );
	glLightfv( GL_LIGHT0, GL_SPOT_DIRECTION, WorldLightDirection );
	glEnable( GL_LIGHT0 );
	glColorMaterial( GL_FRONT_AND_BACK, GL_EMISSION );
	glEnable( GL_COLOR_MATERIAL );
	
	glEnableClientState(GL_VERTEX_ARRAY);

	assert( !glGetError( ) );
}

void SetupPixelDescriptor( HDC hDC )
{
	int PixelFormat = 0;

	static PIXELFORMATDESCRIPTOR pfd = {
		sizeof( PIXELFORMATDESCRIPTOR ),
			1,
			PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_GENERIC_ACCELERATED|
			PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
			32,
			0, 0, 0, 0, 0, 0,
			0,
			0,
			0,
			0, 0, 0, 0,
			16,
			0,
			0,
			PFD_MAIN_PLANE,
			0,
			0, 0, 0
	};

	PixelFormat = ChoosePixelFormat( hDC, &pfd );
	SetPixelFormat( hDC, PixelFormat, &pfd );
}

bool GoFullscreen( int ScreenWidth, int ScreenHeight, int ScreenBpp )
{
	DEVMODE devModeScreen;
	memset( &devModeScreen, 0, sizeof( devModeScreen ) );
	devModeScreen.dmSize = sizeof( devModeScreen );
	devModeScreen.dmPelsWidth = ScreenWidth;
	devModeScreen.dmPelsHeight = ScreenHeight;
	devModeScreen.dmBitsPerPel = ScreenBpp;
	devModeScreen.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT | DM_BITSPERPEL;

	if( ChangeDisplaySettings( &devModeScreen, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL )
	{
		fullscreen = false;
		return false;
	}

	return true;
}



int extensionInit( )
{
	char Extension_Name[ ] = "EXT_fog_coord";

	// Allocate Memory For Our Extension String
	char* glextstring=(char *)malloc( strlen( (char *) glGetString(GL_EXTENSIONS) ) + 1 );
	strcpy ( glextstring, (char *) glGetString( GL_EXTENSIONS ) );		// Grab The Extension List, Store In glextstring

	if (!strstr(glextstring,Extension_Name))				// Check To See If The Extension Is Supported
		return FALSE;							// If Not, Return FALSE

	free( glextstring );							// Free Allocated Memory
	// Setup And Enable glFogCoordEXT
	glFogCoordfEXT = (PFNGLFOGCOORDFEXTPROC) wglGetProcAddress( "glFogCoordfEXT" );

	return TRUE;
}