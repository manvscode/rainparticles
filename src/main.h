#pragma 
#ifndef __MAIN_H__
#define __MAIN_H__
/*
 *	main.h
 *	
 *	Coded by Joseph A. Marrero
 */
#include "opengl.h"
#include "ImageIO/ImageIO.h"
#include "Math/Vector3.h"


void GLInitialize( );
void SetupPixelDescriptor(HDC hDC);
bool GoFullscreen(int ScreenWidth, int ScreenHeight, int ScreenBpp);
LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
int extensionInit( );

#define RESOLUTION_X		1024
#define RESOLUTION_Y		768
#define BIT_RATE			16
#define WORLD_DIMENSION		30

#define GL_FOG_COORDINATE_SOURCE_EXT	0x8450					// Value Taken From GLEXT.H
#define GL_FOG_COORDINATE_EXT		0x8451					// Value Taken From GLEXT.H


#ifdef _DEBUG
	bool fullscreen = FALSE;
#else
	bool fullscreen = TRUE;
#endif

bool keys[ 256 ];
HDC		hDC;

#define		EIGHTY_FPS				(1000.0f / 80.0f)
#define		SIXTY_FPS				(1000.0f / 60.0f)
#define		THIRTY_FPS				(1000.0f / 30.0f)
#define		TWENTYFOUR_FPS			(1000.0f / 24.0f)



//The Targa file that contains the rain texture
#ifdef _DEBUG
#define RAINTEXTURE_FILE "textures/RainDrop.dat"
#else
#define RAINTEXTURE_FILE "RainDrop.dat"
#endif
ImageIO::TGAFILE RainTextureTGAFile;
GLuint RainTextureName = 0;
GLfloat WorldLight[ 4 ] = { 0.2f, 0.2f, 0.2f, 0.0f };
GLfloat WorldLightDirection[ 4 ] = { -2.5f, -1.0f, -2.01f, 0.0f };
GLfloat WorldLightPosition[ 4 ] = { 50.0f, 60.0f, 50.0f, 1.0f };

typedef void (APIENTRY * PFNGLFOGCOORDFEXTPROC) (GLfloat coord);		// Declare Function Prototype
PFNGLFOGCOORDFEXTPROC glFogCoordfEXT = NULL;		

GLfloat fogColor[ 4 ] = { 0.2f, 0.2f, 0.5f, 1.0f };

#endif